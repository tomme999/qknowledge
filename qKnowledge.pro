QT += quick sql svg charts
QT += quickcontrols2
//QT += org.kde.kirigami
//QMAKE_MOC_OPTIONS += -Muri=org.kde.kirigami
//QML2_IMPORT_PATH += $$[QT_INSTALL_QML]/org/kde/kirigami.2
CONFIG += c++14
QMAKE_CXXFLAGS += -std=c++1y
QTPLUGIN += qsqlite@

RC_ICONS = assets/icons/knowledge.ico

android {
QT += androidextras
ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
deployment.path = /assets
deployment.files += dbKnowledge.db3
INSTALLS += deployment
}
# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    business/todo.cpp \
        main.cpp \
    business/question.cpp \
    business/contact.cpp \
    business/task.cpp \
    business/risk.cpp \
    core/business.cpp \
    business/incident.cpp \
    business/project.cpp \
    core/dbservicehelper.cpp \
    models/todomodel.cpp \
    services/contactservice.cpp \
    services/dbcontext.cpp \
    models/contactmodel.cpp \
    repositories/modelrepository.cpp \
    viewModels/contactviewmodel.cpp \
    viewModels/projectviewmodel.cpp \
    models/projectmodel.cpp \
    viewModels/projectlistviewmodel.cpp \
    models/projectlistmodel.cpp \
    models/contactlistmodel.cpp \
    models/incidentmodel.cpp \
    models/taskmodel.cpp \
    models/questionmodel.cpp \
    models/riskmodel.cpp \
    viewModels/contactlistviewmodel.cpp \
    viewModels/riskviewmodel.cpp \
    viewModels/questionviewmodel.cpp \
    repositories/constantrepository.cpp \
    models/questionlistmodel.cpp \
    models/risklistmodel.cpp \
    models/tasklistmodel.cpp \
    viewModels/questionlistviewmodel.cpp \
    viewModels/risklistviewmodel.cpp \
    viewModels/taskviewmodel.cpp \
    viewModels/tasklistviewmodel.cpp \
    business/ext/businessproject.cpp \
    models/projectdetailsmodel.cpp \
    viewModels/projectdetailviewmodel.cpp \
    core/validators/qdatevalidator.cpp \
    core/validators/qdatetimevalidator.cpp \
    viewModels/mainviewmodel.cpp \
    repositories/viewmodelrepository.cpp \
    viewModels/todoviewmodel.cpp \
    viewModels/utils/viewmodelsbridge.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    business/question.h \
    business/contact.h \
    business/rules/todorules.h \
    business/task.h \
    business/risk.h \
    business/todo.h \
    core/business.h \
    business/incident.h \
    business/project.h \
    core/businessrules.h \
    core/dbservice.h \
    core/dbservicehelper.h \
    models/todomodel.h \
    services/contactservice.h \
    services/dbcontext.h \
    business/rules/contactrules.h \
    business/rules/questionrules.h \
    QLinq/qlinq.h \
    QLinq/qlinq2q.h \
    QLinq/qlinq2r.h \
    QLinq/qlinq2t.h \
    models/contactmodel.h \
    repositories/modelrepository.h \
    core/repository.h \
    viewModels/contactviewmodel.h \
    core/viewmodel.h \
    business/rules/projectrules.h \
    business/rules/riskrules.h \
    business/rules/taskrules.h \
    viewModels/projectviewmodel.h \
    models/projectmodel.h \
    viewModels/projectlistviewmodel.h \
    models/projectlistmodel.h \
    models/contactlistmodel.h \
    models/incidentmodel.h \
    models/taskmodel.h \
    models/questionmodel.h \
    models/riskmodel.h \
    viewModels/contactlistviewmodel.h \
    viewModels/riskviewmodel.h \
    viewModels/questionviewmodel.h \
    repositories/constantrepository.h \
    models/questionlistmodel.h \
    models/risklistmodel.h \
    models/tasklistmodel.h \
    viewModels/questionlistviewmodel.h \
    viewModels/risklistviewmodel.h \
    viewModels/taskviewmodel.h \
    viewModels/tasklistviewmodel.h \
    business/ext/businessproject.h \
    business/ext/businessprojectrules.h \
    business/ext/dbserviceproject.h \
    models/projectdetailsmodel.h \
    viewModels/projectdetailviewmodel.h \
    core/validators/qdatevalidator.h \
    core/validators/qdatetimevalidator.h \
    viewModels/mainviewmodel.h \
    repositories/viewmodelrepository.h \
    viewModels/todoviewmodel.h \
    viewModels/utils/viewmodelsbridge.h

DISTFILES += \
    sqlite/sqliteScript.sql \
    android-sources/AndroidManifest.xml


# win32:CONFIG(release, debug|release): LIBS += -L$$PWD/build-kirigami-Desktop_Qt_5_12_1_GCC_64bit2-Debug/org/kde/kirigami.2/release/ -lkirigamiplugin
# else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/build-kirigami-Desktop_Qt_5_12_1_GCC_64bit2-Debug/org/kde/kirigami.2/debug/ -lkirigamiplugin
# else:unix: LIBS += -L$$PWD/build-kirigami-Desktop_Qt_5_12_1_GCC_64bit2-Debug/org/kde/kirigami.2/ -lkirigamiplugin

# INCLUDEPATH += $$PWD/build-kirigami-Desktop_Qt_5_12_1_GCC_64bit2-Debug/org/kde/kirigami.2
# DEPENDPATH += $$PWD/build-kirigami-Desktop_Qt_5_12_1_GCC_64bit2-Debug/org/kde/kirigami.2

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/build-kirigami-Desktop_Qt_5_12_1_GCC_64bit2-Debug/org/kde/kirigami.2/release/ -lkirigamiplugin
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/build-kirigami-Desktop_Qt_5_12_1_GCC_64bit2-Debug/org/kde/kirigami.2/debug/ -lkirigamiplugin
#else:unix: LIBS += -L$$PWD/build-kirigami-Desktop_Qt_5_12_1_GCC_64bit2-Debug/org/kde/kirigami.2/ -lkirigamiplugin


#INCLUDEPATH += $$PWD/build-kirigami-Desktop_Qt_5_12_1_GCC_64bit2-Debug/org/kde/kirigami.2
#DEPENDPATH += $$PWD/build-kirigami-Desktop_Qt_5_12_1_GCC_64bit2-Debug/org/kde/kirigami.2
