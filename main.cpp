#include <QDebug>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtSql/QSqlDatabase>
#include <QLinq/qlinq2q.h>
#include <QSharedPointer>
#include <QQuickStyle>
#include <QQmlContext>

#include "services/dbcontext.h"
#include "viewModels/contactviewmodel.h"
#include "viewModels/projectviewmodel.h"
#include "viewModels/projectlistviewmodel.h"
#include "viewModels/contactlistviewmodel.h"
#include "viewModels/questionviewmodel.h"
#include "viewModels/questionlistviewmodel.h"
#include "viewModels/riskviewmodel.h"
#include "viewModels/risklistviewmodel.h"
#include "viewModels/taskviewmodel.h"
#include "viewModels/tasklistviewmodel.h"
#include "viewModels/projectdetailviewmodel.h"
#include "viewModels/mainviewmodel.h"
#include "viewModels/todoviewmodel.h"

#include "core/validators/qdatevalidator.h"
#include "core/validators/qdatetimevalidator.h"



int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qDebug()<<QQuickStyle::name();

    engine.rootContext()->setContextProperty("mainViewModel", MainViewModel::get());

    qmlRegisterType<MainViewModel>("core.backend.viewmodel", 1, 0, "MainViewModel");

    qmlRegisterType<QDateValidator>("core.backend.validator", 1, 0, "DateValidator");
    qmlRegisterType<QDateTimeValidator>("core.backend.validator", 1, 0, "DateTimeValidator");

    qmlRegisterType<ContactViewModel>("core.backend.viewmodel", 1, 0, "ContactViewModel");
    qmlRegisterType<ProjectViewModel>("core.backend.viewmodel", 1, 0, "ProjectViewModel");
    qmlRegisterType<ProjectListViewModel>("core.backend.viewmodel", 1, 0, "ProjectListViewModel");
    qmlRegisterType<ContactListViewModel>("core.backend.viewmodel", 1, 0, "ContactListViewModel");
    qmlRegisterType<QuestionViewModel>("core.backend.viewmodel", 1, 0, "QuestionViewModel");
    qmlRegisterType<QuestionListViewModel>("core.backend.viewmodel", 1, 0, "QuestionListViewModel");
    qmlRegisterType<RiskViewModel>("core.backend.viewmodel", 1, 0, "RiskViewModel");
    qmlRegisterType<RiskListViewModel>("core.backend.viewmodel", 1, 0, "RiskListViewModel");
    qmlRegisterType<TaskViewModel>("core.backend.viewmodel", 1, 0, "TaskViewModel");
    qmlRegisterType<TaskListViewModel>("core.backend.viewmodel", 1, 0, "TaskListViewModel");
    qmlRegisterType<ProjectDetailViewModel>("core.backend.viewmodel", 1, 0, "ProjectDetailViewModel");
    qmlRegisterType<TodoViewModel>("core.backend.viewmodel", 1, 0, "TodoViewModel");

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
