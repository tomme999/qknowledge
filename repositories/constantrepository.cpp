#include "constantrepository.h"

ConstantRepository ConstantRepository::m_ConstantRepository = ConstantRepository();

ConstantRepository::ConstantRepository()
{


    m_lstColors << QColor("#aaaaaa");
    m_lstColors << QColor("#aaaa55");
    m_lstColors << QColor("#aa55aa");
    m_lstColors << QColor("#aa5555");
    m_lstColors << QColor("#55aaaa");
    m_lstColors << QColor("#55aa55");
    m_lstColors << QColor("#5555aa");
    m_lstColors << QColor("#555555");

    m_lstRiskColors << QColor("#FF555555");
    m_lstRiskColors << QColor("#FF00AA00");
    //m_lstRiskColors << QColor("#FF55FF00");
    //m_lstRiskColors << QColor("#FF99FF00");
    //m_lstRiskColors << QColor("#FFCCFF00");
    m_lstRiskColors << QColor("#FFAAAA00");
    //m_lstRiskColors << QColor("#FFFFCC00");
    //m_lstRiskColors << QColor("#FFFF9900");
    //m_lstRiskColors << QColor("#FFFF5500");
    //m_lstRiskColors << QColor("#FFFF3300");
    m_lstRiskColors << QColor("#FFAA0000");
}
