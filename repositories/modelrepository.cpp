#include "modelrepository.h"
ModelRepository ModelRepository::m_ModelRepository = ModelRepository();

ModelRepository::ModelRepository()
{

}

template<> ContactModel      *   ModelRepository::get<ContactModel>         ( QObject *parent ) { return new ContactModel(parent);        }
template<> ContactListModel  *   ModelRepository::get<ContactListModel>     ( QObject *parent ) { return new ContactListModel(parent);    }
template<> ProjectModel      *   ModelRepository::get<ProjectModel>         ( QObject *parent ) { return new ProjectModel(parent);        }
template<> ProjectListModel  *   ModelRepository::get<ProjectListModel>     ( QObject *parent ) { return new ProjectListModel(parent);    }
template<> RiskModel         *   ModelRepository::get<RiskModel>            ( QObject *parent ) { return new RiskModel(parent);           }
template<> RiskListModel     *   ModelRepository::get<RiskListModel>        ( QObject *parent ) { return new RiskListModel(parent);       }
template<> TaskModel         *   ModelRepository::get<TaskModel>            ( QObject *parent ) { return new TaskModel(parent);           }
template<> TaskListModel     *   ModelRepository::get<TaskListModel>        ( QObject *parent ) { return new TaskListModel(parent);       }
template<> QuestionModel     *   ModelRepository::get<QuestionModel>        ( QObject *parent ) { return new QuestionModel(parent);       }
template<> QuestionListModel *   ModelRepository::get<QuestionListModel>    ( QObject *parent ) { return new QuestionListModel(parent);   }
template<> ProjectDetailsModel * ModelRepository::get<ProjectDetailsModel>  ( QObject *parent ) { return new ProjectDetailsModel(parent); }
template<> TodoModel         *   ModelRepository::get<TodoModel>            ( QObject *parent ) { return new TodoModel(parent);           }
