#ifndef MODELREPOSITORY_H
#define MODELREPOSITORY_H

#include <QObject>
#include <core/repository.h>
#include <models/contactmodel.h>
#include <models/projectmodel.h>
#include <models/questionmodel.h>
#include <models/questionlistmodel.h>
#include <models/riskmodel.h>
#include <models/risklistmodel.h>
#include <models/taskmodel.h>
#include <models/tasklistmodel.h>
#include <models/todomodel.h>
#include <models/projectlistmodel.h>
#include <models/contactlistmodel.h>
#include <models/projectdetailsmodel.h>

class ModelRepository : core::Repository
{
private:
    explicit ModelRepository();

    static ModelRepository m_ModelRepository;

public:
    static ModelRepository      instance ()                   {   return m_ModelRepository;                     }

    template <class A> A*       get (QObject *parent)         {   Q_UNUSED(parent); qFatal("Model unknown... Define it in ModelRepository...");return nullptr;             }
};

template<> ContactModel        * ModelRepository::get<ContactModel>          ( QObject *parent );
template<> ContactListModel    * ModelRepository::get<ContactListModel>      ( QObject *parent );
template<> ProjectModel        * ModelRepository::get<ProjectModel>          ( QObject *parent );
template<> ProjectListModel    * ModelRepository::get<ProjectListModel>      ( QObject *parent );
template<> QuestionModel       * ModelRepository::get<QuestionModel>         ( QObject *parent );
template<> QuestionListModel   * ModelRepository::get<QuestionListModel>     ( QObject *parent );
template<> RiskModel           * ModelRepository::get<RiskModel>             ( QObject *parent );
template<> RiskListModel       * ModelRepository::get<RiskListModel>         ( QObject *parent );
template<> TaskModel           * ModelRepository::get<TaskModel>             ( QObject *parent );
template<> TaskListModel       * ModelRepository::get<TaskListModel>         ( QObject *parent );
template<> ProjectDetailsModel * ModelRepository::get<ProjectDetailsModel>   ( QObject *parent );
template<> TodoModel           * ModelRepository::get<TodoModel>             ( QObject *parent );

#endif // MODELREPOSITORY_H
