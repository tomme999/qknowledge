#ifndef CONSTANTREPOSITORY_H
#define CONSTANTREPOSITORY_H

#include <core/repository.h>
#include <QColor>
#include <QList>

class ConstantRepository : core::Repository
{
private :
    explicit ConstantRepository();

    static ConstantRepository   m_ConstantRepository;
    QList<QColor>               m_lstColors;
    QList<QColor>               m_lstRiskColors;

public:
    static ConstantRepository    instance ()            {   return ConstantRepository::m_ConstantRepository;        }

    QColor getColorFromId ( long id )                   {   return m_lstColors[abs(id % m_lstColors.count())];      }
    QColor getRiskColor   ( int level, int probability ){   long a=(level*probability)/3; return  m_lstRiskColors[abs(a % m_lstRiskColors.count())]; }
};

#endif // CONSTANTREPOSITORY_H
