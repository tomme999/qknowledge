#ifndef VIEWMODELREPOSITORY_H
#define VIEWMODELREPOSITORY_H

#include <QObject>
#include <core/repository.h>
#include <viewModels/utils/viewmodelsbridge.h>

class ViewModelRepository : public core::Repository
{

    static ViewModelRepository   *m_ViewModelRepository;
    static ViewModelsBridge      *m_ViewModelsBridge;

public:
    explicit ViewModelRepository();

    static ViewModelRepository      instance ()
    {
        if(m_ViewModelRepository==nullptr)
            ViewModelRepository::m_ViewModelRepository = new ViewModelRepository();
        return *m_ViewModelRepository;
    }

    ViewModelsBridge                *bridge ()
    {
        if(m_ViewModelsBridge==nullptr)
            ViewModelRepository::m_ViewModelsBridge = new ViewModelsBridge();
        return m_ViewModelsBridge;
    }
signals:

public slots:
};

#endif // VIEWMODELREPOSITORY_H
