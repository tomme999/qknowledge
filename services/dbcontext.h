#ifndef DBCONTEXT_H
#define DBCONTEXT_H

#include <QObject>

#include "contactservice.h"
#include <business/incident.h>
#include <business/project.h>
#include <business/question.h>
#include <business/risk.h>
#include <business/task.h>
#include <business/todo.h>
#include <core/dbservice.h>
#include <business/ext/dbserviceproject.h>

typedef  DBService<Incident>            IncidentService;
typedef  DBService<Project>             ProjectService;
typedef  DBServiceProject<Question>     QuestionService;
typedef  DBServiceProject<Risk>         RiskService;
typedef  DBServiceProject<Task>         TaskService;
typedef  DBServiceProject<Todo>         TodoService;

class DbContext
{
private:
    DbContext();

public:


    static DbContext get()  { if (DbContext::m_dbContext==nullptr) DbContext::m_dbContext = new DbContext(); return *DbContext::m_dbContext; }

    auto Contacts()         {   return m_Contacts;  }
    auto Incidents()        {   return m_Incidents; }
    auto Projects()         {   return m_Projects;  }
    auto Questions()        {   return m_Questions; }
    auto Risks()            {   return m_Risks;     }
    auto Tasks()            {   return m_Tasks;     }
    auto Todos()            {   return m_Todos;     }

protected:
    static DbContext        *m_dbContext ;

    ContactService          m_Contacts;
    IncidentService         m_Incidents;
    ProjectService          m_Projects;
    QuestionService         m_Questions;
    RiskService             m_Risks;
    TaskService             m_Tasks;
    TodoService             m_Todos;
};

#endif // DBCONTEXT_H
