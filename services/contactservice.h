#ifndef CONTACTSERVICE_H
#define CONTACTSERVICE_H

#include <QObject>
#include "core/dbservice.h"
#include "business/contact.h"

class ContactService : public DBService<Contact>
{
public:
    ContactService();
};

#endif // CONTACTSERVICE_H
