#include "dbcontext.h"
#include <QDebug>
#include <QFile>
#include <QString>
#include <QSqlDatabase>
#include <QMessageBox>
#ifdef __ANDROID__
#include <QtAndroidExtras>
#endif

DbContext  *DbContext::m_dbContext = nullptr;

DbContext::DbContext()
{
#ifdef __ANDROID__
    QString path1  = QStandardPaths::writableLocation(QStandardPaths::StandardLocation::AppDataLocation);
    QString path = path1 +"/dbKnowledge.db3";
#else
#ifdef __unix__
    QString path("/media/thierry/ProjectDiskN/Thierry/Qt/qKnowledge/android/assets/dbKnowledge.db3");
#else
    QString path("C:/data/Personnel/Sources/qknowledge/android/assets/dbKnowledge.db3");
#endif
#endif
    QFile dfile(path);
    if (!dfile.exists())
    {
         if(!QFile("assets:/dbKnowledge.db3").exists())
            qWarning("La base n'existe pas !");
         if (!QFile::copy("assets:/dbKnowledge.db3", path))
                 qWarning("Pb de copie de base !");
         QFile::setPermissions(path,QFile::WriteOwner | QFile::ReadOwner);
    }

    auto m_db = QSqlDatabase::addDatabase("QSQLITE");

    //m_db.setDatabaseName("C:\\data\\dbKnowledge.db3");
    //m_db.setDatabaseName("/media/thierry/ProjectDiskN/Thierry/Qt/qKnowledge/dbKnowledge.db3");
    m_db.setDatabaseName(path);//"./dbKnowledge.db3");

    if (!m_db.open())
    {
       qDebug() << "Error: connection with database fail";
    }
    else
    {
       qDebug() << "Database: connection ok";
    }
}
