﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Data.Common;
using Models;

namespace DAL
{
    public class KnowledgeDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL("server=192.168.1.38;database=knowledge;user=knowledge;password=Knowledge59!");
        }

        public DbSet<Contact> Contacts
        {
            get;
            set;
        }

        //public KnowledgeDbContext() : base("name=KnowledgeDbContexte") { }
    }
}
