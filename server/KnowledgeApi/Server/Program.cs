﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Server
{
    // Source https://www.c-sharpcorner.com/article/create-net-core-web-api-c-sharp-in-monodevelop-on-ubuntu-18-04/
    // Installation du dotnet Core 
    //    wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb  
    // sudo dpkg -i packages-microsoft-prod.deb
    // sudo add-apt-repository universe
    // sudo apt-get install apt-transport-https
    // sudo apt-get update
    // sudo apt-get install dotnet-sdk-2.2  
    // dotnet –version
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
