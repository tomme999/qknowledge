﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore;

using DAL;
using Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Server
{
    [Route("api/[controller]")]
    public class ContactController : Controller
    {
        /*private readonly DAL.KnowledgeDbContext _knowledgeDbContext;
        public ContactController(KnowledgeDbContext knowledgeDbContext)
        {
            _knowledgeDbContext = knowledgeDbContext;
        }*/

        // GET: api/values
        [HttpGet]
        public IEnumerable<Models.Contact> Get()
        {
            var dbContext = new KnowledgeDbContext();

            //var contacts = dbContext.Contacts;//.Where(a => a.Id == 1);
            var contacts = dbContext.Contacts.FromSql("call knowledge.sp_select_all_contact();");

            return contacts;
/*                        return new Contact[] {
                            new Contact { Id = 1, name="MyName", firstname="Firstname", eMail="email", phoneNumber="+33"},
                            new Contact { Id = 1, name="MyName", firstname="Firstname", eMail="email", phoneNumber="+33"}
                            };*/
            /*return await _knowledgeDbContext.Contacts.FromSql("sp_select_all_contact").ToArrayAsync();*/
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Contact Get(int id)
        {
            var dbContext = new KnowledgeDbContext();

            var contact = dbContext.Contacts.FirstOrDefault(a => a.Id == id);

            return contact;
            //return new Contact { Id = 1, name="MyName", firstname="Firstname", eMail="email", phoneNumber="+33"};
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]Contact value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Contact value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
