﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models
{
    [Table("contact")]
    public class Contact
    {
        public Contact()
        {
        }

        [Column("id")]
        public long Id { get; set; }

        public string name { get; set; }
        public string firstname { get; set; }
        public string phoneNumber { get; set; }
        public string eMail { get; set; }
    }
}
