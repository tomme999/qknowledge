import QtQuick 2.4
import QtQuick.Controls 2.5

Item {
    id: root
    width: 400
    height: 400

    property alias model: listView.model
    property bool editMode: false
    signal close;
    signal store;

    ListView {
        id: listView
        anchors.rightMargin: 5
        anchors.leftMargin: 5
        anchors.bottomMargin: 5
        anchors.topMargin: 5
anchors.fill:parent;
//        anchors.top: toolbar.bottom
//        anchors.bottom: parent.bottom
//        anchors.left: parent.left
//        anchors.right: parent.right
        clip: true
        spacing: 5
        model: itemModel
    }

    RoundButton {
        id: rndButEdit
        text: "+"

        anchors.right: parent.right
        anchors.rightMargin: 5
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        display: AbstractButton.IconOnly
        highlighted: true
        icon.source: "qrc:/assets/icons/edit.svg"
        visible: !root.editMode
    }

    RoundButton {
        id: rndButStore
        x: 165
        y: 309
        anchors.right: parent.right
        anchors.rightMargin: 5
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        display: AbstractButton.IconOnly
        highlighted: true
        icon.source: "qrc:/assets/icons/save.svg"
        visible: root.editMode
    }

    RoundButton {
        id: rndButClose
        x: 165
        y: 309
        anchors.right: parent.right
        anchors.rightMargin: 5
        anchors.top: parent.top
        anchors.topMargin: 5
        display: AbstractButton.IconOnly
        highlighted: false
        icon.source: "qrc:/assets/icons/delete.svg"
    }

    Connections {
        target: rndButEdit
        onClicked: root.editMode = true
    }

    Connections {
        target: rndButStore
        onClicked: root.store();
    }

    Connections {
        target: rndButClose
        onClicked: root.close();
    }
}
