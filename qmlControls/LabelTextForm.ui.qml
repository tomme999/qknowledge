import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3

Item {
    id: root
    width: 800
    height: 400

    property alias labelText: label.text
    property alias text: textField.text
    property alias validator: textField.validator
    property alias inputMask: textField.inputMask
    property alias editMode: textField.enabled

    Column {
        id: column
        anchors.fill: parent

        Label {
            id: label
            text: qsTr("Label")
            width: root.width
        }

        TextField {
            id: textField
            text: qsTr("Text Field")
            enabled: true
            width: root.width
        }
    }
}
