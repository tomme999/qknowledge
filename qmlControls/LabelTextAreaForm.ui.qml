import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3

Item {
    width: 800
    height: label.height + textArea.height + 2 * root.spacing

    property alias labelText: label.text
    property alias text: textArea.text
    //property alias enabled: textArea.enabled
    property alias editMode: textArea.enabled

    Column {
        id: root
        spacing: 5
        anchors.fill: parent

        Label {
            id: label
            text: qsTr("Label")
            width: root.width
        }


            TextArea {
                id: textArea
                text: qsTr("Text Area")
                enabled: true
                visible: true
            }
    }
}


