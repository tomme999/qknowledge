import QtQuick 2.4
import QtQuick.Controls 2.3
import core.backend.viewmodel 1.0

Page {
    id: root
    width: 300
    height: 200

    title: projectDetailViewModel.projectViewModel.name + " : " + activeView

    property string activeView: qsTr("Questions")
    property ProjectDetailViewModel projectDetailViewModel: ProjectDetailViewModel {
    }

    SwipeView {
        id: swipeView
        currentIndex: tabBar.currentIndex //pageIndicator.currentIndex
        anchors.fill: parent

        Item {
            height: root.height
            width: root.width
            QuestionListViewForm {
                id: questionListView
                anchors.fill: parent
                questionListViewModel: projectDetailViewModel.questionListViewModel
            }
        }

        Item {
            height: root.height
            width: root.width
            TaskListViewForm {
                id: taskListView
                anchors.fill: parent
                taskListViewModel: projectDetailViewModel.taskListViewModel
            }
        }

        Item {
            height: root.height
            width: root.width
            RiskListViewForm {
                id: riskListView
                anchors.fill: parent
                riskListViewModel: projectDetailViewModel.riskListViewModel
            }
        }
        Item {
            height: root.height
            width: root.width
        }
        Item {
            height: root.height
            width: root.width
            Label {
                anchors.centerIn: parent
                text: "TODO LIST... TO DO ;-)"
            }
        }
    }
    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            id: tabButtonQuestions
            text: qsTr("Questions")
            icon.source: "qrc:/assets/icons/questions.svg"
        }
        TabButton {
            id: tabButtonTasks
            text: qsTr("Tâches")
            icon.source: "qrc:/assets/icons/tasks.svg"
        }
        TabButton {
            id: tabButtonRisks
            text: qsTr("Risques")
            icon.source: "qrc:/assets/icons/risks.svg"
        }
        TabButton {
            id: tabButtonIncidents
            text: qsTr("Incidents")
            icon.source: "qrc:/assets/icons/incident.svg"
        }
        TabButton {
            id: tabButtonTodo
            text: qsTr("ToDo")
            icon.source: "qrc:/assets/icons/todo.svg"
        }
    }

    //    PageIndicator {
    //        id: pageIndicator
    //        currentIndex: swipeView.currentIndex
    //        interactive: true

    //        count: swipeView.count

    //        anchors.bottom: swipeView.bottom
    //        anchors.horizontalCenter: parent.horizontalCenter
    //    }
    Connections {
        target: tabBar
        onCurrentIndexChanged: {
            switch (tabBar.currentIndex) {
            case 0:
                root.activeView = qsTr("Questions")
                break
            case 1:
                root.activeView = qsTr("Tâches")
                break
            case 2:
                root.activeView = qsTr("Risques")
                break
            case 3:
                root.activeView = qsTr("Incidents")
                break
            case 4:
                root.activeView = qsTr("ToDo List")
                break
            default:
                root.activeView = qsTr("????")
            }
        }
    }

    //    Connections {
    //        target: tabButtonQuestions
    //        onClicked: questionListViewForm.questionListViewModel.load(-1)
    //    }
    //    Connections {
    //        target: tabButtonTasks
    //        onClicked: taskListViewForm.taskListViewModel.load(-1)
    //    }
    //    Connections {
    //        target: tabButtonRisks
    //        onClicked: riskListViewForm.riskListViewModel.load(-1)
    //    }
}
