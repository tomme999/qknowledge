import QtQuick 2.4
import core.backend.viewmodel 1.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Item {
    id: root
    width: 400
    height: Qt.application.font.pixelSize * 2.4 + 4
    property ContactViewModel contactViewModel: null
    property color background: "#FF0000"

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent
        spacing: 1

        RowLayout {
            id: rowLayout
            width: 100
            spacing: 1

            Rectangle {
                color: contactViewModel.backgroundColor
                radius: height / 2
                width: Qt.application.font.pixelSize * 2.4
                height: Qt.application.font.pixelSize * 2.4

                Text {
                    id: element
                    color: "#ffffff"
                    text: contactViewModel.initials
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.fill: parent

                    font.pixelSize: Qt.application.font.pixelSize * 1.2
                }
            }
            ColumnLayout {
                spacing: 1
                Label {
                    id: lblFullName
                    width: parent.width
                    font.pixelSize: Qt.application.font.pixelSize
                    text: contactViewModel.firstName + " " + contactViewModel.name
                    Layout.fillWidth: true
                    elide: Text.ElideRight
                }

                Label {
                    id: lblmail
                    width: parent.width
                    font.pixelSize: Qt.application.font.pixelSize
                    text: contactViewModel.email
                    Layout.fillWidth: true
                    elide: Text.ElideRight
                }
            }
        }

        Rectangle {
            id: rectangle
            height: 1
            color: "#555555"
            Layout.fillWidth: true
            Layout.fillHeight: false
        }
    }
}




/*##^## Designer {
    D{i:1;anchors_height:100;anchors_width:100;anchors_x:125;anchors_y:69}
}
 ##^##*/
