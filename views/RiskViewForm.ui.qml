import QtQuick 2.4
import core.backend.viewmodel 1.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12

Item {
    width: 400
    height: 40
    property RiskViewModel riskViewModel: null

    RowLayout {
        id: rowLayout
        anchors.fill: parent
        spacing: 4

        Item {
            id: name
            width: 32
            height: 32
            Rectangle {
                width: 32
                height: 32
                color :riskViewModel.colorRisk
                radius: height/4
                Image {
                    id: image
                    width: 32
                    height: 32
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/assets/icons/risks.svg"
                }

                ColorOverlay {
                    anchors.fill: image
                    source: image
                    color: "#ffffffff"
                }
            }
        }

        Column {
            id: column
            width: 200
            height: 400
            Layout.fillWidth: true

            Label {
                id: lblName
                text: riskViewModel.name
                font.pixelSize: Qt.application.font.pixelSize
            }

            Label {
                id: lblDescription
                text: riskViewModel.description
                font.pixelSize: Qt.application.font.pixelSize
            }
        }
    }
}
