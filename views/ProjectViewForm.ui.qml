import QtQuick 2.4
import core.backend.viewmodel 1.0
import QtQuick.Controls 2.5
import QtQuick.Controls.impl 2.12
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12
import QtQuick.Controls.Material 2.12

Item {
    id: root
    width: 400
    height: 100
    property alias dropShadow: dropShadow
    property color textColor: "#FFFFFFFF"
    property ProjectViewModel projectViewModel: null
    property color tileColor: (projectViewModel
                               == null) ? "#AA5555" : projectViewModel.backgroundColor //"#AA5555"
    property bool isSelected: false
    property bool isNormal: true
    signal clicked(int projectId)
    signal doubleClicked(int projectId)

    opacity: (root.isNormal || root.isSelected) ? 1.0 : 0.4

    Rectangle {
        id: rectangle
        //color: root.tileColor
        color: Material.backgroundColor //"#FFFFFF"
        radius: 14
        anchors.fill: parent

        MouseArea {
            id: mouseArea
            anchors.fill: parent
        }

        RowLayout {
            id: rowLayout
            anchors.fill: parent

            Rectangle {
                id: icon
                width: icon.height
                height: root.height - 4
                color: root.tileColor //root.textColor
                radius: height / 2
                //border.width: 2
                Layout.preferredHeight: height
                Layout.preferredWidth: width
                Label {
                    id: element
                    color: root.textColor //root.tileColor
                    text: projectViewModel.initials
                    //font.family: "Noto Sans"
                    font.bold: true
                    font.pixelSize: parent.height / 2
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                }
            }

            ColumnLayout {
                id: columnLayout
                Layout.fillWidth: true
                antialiasing: false
                spacing: 1
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                Layout.fillHeight: true

                //anchors.right: parent.right
                //anchors.left: icon.right
                Label {
                    id: txtTitle

                    //color: root.textColor
                    //font.family: "Noto Sans"
                    text: projectViewModel.name
                    Layout.fillWidth: true

                    elide: Text.ElideRight
                    font.bold: true
                    font.pointSize: root.height / 5

                    width: columnLayout.width
                }

                Label {
                    id: txtDescription
                    //color: root.textColor
                    //font.family: "Noto Sans"
                    elide: Text.ElideRight
                    font.pointSize: root.height / 6
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    wrapMode: Text.WordWrap
                    height: columnLayout.height
                    width: columnLayout.width
                    text: projectViewModel.description
                }
            }
        }

        ToolButton {
            id: toolButtonDelete
            width: 32
            height: 32
            display: AbstractButton.IconOnly
            padding: 2
            icon.source: "qrc:/assets/icons/trash.svg"
            anchors.margins: 0
            anchors.right: rectangle.right
            anchors.top: rectangle.top
            visible: root.isSelected
        }
        ToolButton {
            id: toolButtonEdit
            width: 32
            height: 32
            font.pointSize: 9
            display: AbstractButton.IconOnly
            padding: 2
            icon.source: "qrc:/assets/icons/edit.svg"
            anchors.margins: 0
            anchors.right: toolButtonDelete.left
            anchors.top: rectangle.top
            visible: root.isSelected
        }
    }

    DropShadow {
        id: dropShadow
        visible: isSelected
        anchors.fill: rectangle
        horizontalOffset: 3
        verticalOffset: 3
        radius: 8.0
        samples: 17
        color: "#80000000"
        source: rectangle
    }

    Connections {
        target: mouseArea
        onClicked: root.clicked(projectViewModel.id)
    }

    Connections {
        target: mouseArea
        onDoubleClicked: root.doubleClicked(projectViewModel.id)
    }

    Connections {
        target: mouseArea
        onPressAndHold: root.doubleClicked(projectViewModel.id)
    }
}












/*##^## Designer {
    D{i:11;invisible:true}
}
 ##^##*/
