import QtQuick 2.4
import QtQuick.Controls 2.3
import core.backend.viewmodel 1.0

Page {

    width: 400
    height: 400
    property ContactListViewModel contactListViewModel: ContactListViewModel {
    }
    ListView {
        anchors.fill: parent
        model: contactListViewModel.contactListViewModel
        delegate: ContactViewForm {
            id: contactViewForm
            contactViewModel: contactListViewModel.contactListViewModel[index]
            width: parent.width
        }


    }
}
