import QtQml.Models 2.12
import QtQuick 2.4
import QtQuick.Controls 2.3
import "../qmlControls"
import core.backend.viewmodel 1.0
import core.backend.validator 1.0

Dialog {
    id: root
    width: 300
    height: 400
    property QuestionViewModel questionViewModel: null
    signal close

    ObjectModel {
        id: itemModel
        LabelTextArea {
            id: lbltxtQuestion
            text: questionViewModel.question
            labelText: "Question"
        }

        LabelTextArea {
            id: lbltxtAnswer
            text: questionViewModel.answer
            labelText: qsTr("Réponse")
        }

        LabelText {
            id: lbltxtWanted
            text: questionViewModel.wanted
            labelText: "Date souhaitée de réponse"
            height: 50
            inputMask: qsTr("99/99/9999")
            validator: DateValidator {
            }
        }

        LabelText {
            id: lbltxtAnswerDate
            text: questionViewModel.answerDate
            labelText: qsTr("Date de la réponse")
            height: 50
            inputMask: qsTr("99/99/9999")
            validator: DateValidator {
            }
        }
    }

    header: ToolBar {
        id: toolbar
        contentHeight: toolButton.implicitHeight
        ToolButton {
            id: toolButtonQuestion
            icon.source: "qrc:/assets/icons/questions.svg"
            anchors.left: parent.left
        }

        Label {
            text: "Question"
            anchors.centerIn: parent
        }
        ToolButton {
            id: toolButtonClose
            highlighted: none.none
            icon.source: "qrc:/assets/icons/delete.svg"
            anchors.right: parent.right
        }
    }
    contentItem: ListView {
        anchors.rightMargin: 5
        anchors.leftMargin: 5
        anchors.bottomMargin: 5
        anchors.topMargin: 5
        anchors.top: toolbar.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        clip: true
        spacing: 5
        model: itemModel
    }
    Connections {
        target: toolButtonClose
        onClicked: {
            console.debug("youps")
            root.visible = false;
        }
    }
}
