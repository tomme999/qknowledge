import QtQuick 2.4
import QtQuick.Controls 2.3

import core.backend.viewmodel 1.0

Item {
    id: root

    property int selectedId: -1

    //title: qsTr("Liste des questions")
    property QuestionListViewModel questionListViewModel: QuestionListViewModel {
    }

    property alias populate: gridView.populate

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: root.selectedId = -1
    }

    GridView {
        id: gridView
        anchors.fill: parent
        cellWidth: width / Math.round(Math.max((width / 300), 1))
        cellHeight: 52

        model: questionListViewModel.questionListViewModel

        delegate: QuestionViewForm {
            id: questionViewForm
            questionViewModel: questionListViewModel.questionListViewModel[index]
            height: 50

            width: gridView.cellWidth - 2 //root.width/grid.columns-grid.spacing;
        }

        RoundButton {
            id: rndBoutAdd
            text: "+"
            checkable: false
            autoRepeat: false
            highlighted: true
            display: AbstractButton.IconOnly
            icon.source: "qrc:/assets/icons/add2.svg"
            anchors.right: parent.right
            anchors.rightMargin: 8
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 8
            visible: selectedId == -1
        }
    }
    QuestionViewEdit {
        id: popup
        questionViewModel: QuestionViewModel {
        }
        anchors.centerIn: parent
        width: root.width * 0.9
        height: root.height * 0.9
        modal: true
        focus: true
        visible: false

        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
    }

    Connections {
        target: rndBoutAdd
        onClicked: {
            //popup.visible = true
                root.questionListViewModel.edit()
        }
    }
}






/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
