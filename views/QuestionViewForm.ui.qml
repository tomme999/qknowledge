import QtQuick 2.4
import core.backend.viewmodel 1.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.12

Item {
    width: 400
    height: 40
    property QuestionViewModel questionViewModel: null

    RowLayout {
        id: rowLayout
        anchors.fill: parent
        spacing: 4


        /*Item {
            id: name
            width: 32
            height: 32

            Image {
                id: image
                width: 32
                height: 32
                fillMode: Image.PreserveAspectFit
                source: "qrc:/assets/icons/questions.svg"
            }
        }*/
        Item {
            id: name
            width: 32
            height: 32
            Rectangle {
                width: 32
                height: 32
                //color: questionViewModel.colorRisk
                radius: height / 4
                Image {
                    id: image
                    width: 32
                    height: 32
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/assets/icons/questions.svg"

                }

                ColorOverlay {
                    anchors.fill: image
                    source: image
                    color: "#ffff0000"
                }
            }
        }

        Column {
            id: column
            width: 200
            height: 400
            Layout.fillWidth: true

            Label {
                id: lblQuestion
                text: questionViewModel.question
                font.bold: true
                font.pixelSize: Qt.application.font.pixelSize
            }

            Label {
                id: lblAnswer
                text: questionViewModel.answer
                font.pixelSize: Qt.application.font.pixelSize
            }
        }
    }
}






/*##^## Designer {
    D{i:1;anchors_height:100;anchors_width:100;anchors_x:45;anchors_y:175}
}
 ##^##*/
