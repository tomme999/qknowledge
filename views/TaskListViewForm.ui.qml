import QtQuick 2.4
import QtQuick.Controls 2.3
import core.backend.viewmodel 1.0

Item {
    id: root

    property int selectedId: -1

    //title: qsTr("Liste des questions")
    property TaskListViewModel taskListViewModel: TaskListViewModel {

    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: root.selectedId = -1
    }

    GridView {
        id: gridView
        anchors.fill: parent
        cellWidth: width / Math.round(Math.max((width / 300), 1))
        cellHeight: 52

        model: taskListViewModel.taskListViewModel

        delegate: TaskViewForm {
            id: riskViewForm
            taskViewModel: taskListViewModel.taskListViewModel[index]
            height: 50

            width: gridView.cellWidth - 2 //root.width/grid.columns-grid.spacing;
        }
    }
}




/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
