import QtQuick 2.4
import QtQuick.Controls 2.3
import core.backend.viewmodel 1.0

Page {
    id: root

    property int selectedId: -1

    title: qsTr("Liste des projets")
    signal askOpenDetail(int idProject)

    property ProjectListViewModel projectListViewModel: ProjectListViewModel {
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: root.selectedId = -1
    }

    GridView {
        id: gridView
        anchors.fill: parent
        cellWidth: width / Math.round(Math.max((width / 300), 1))
        cellHeight: 52

        model: projectListViewModel.projectListViewModel

        delegate: Item {
            ProjectView {
                id: projectView
                projectViewModel: projectListViewModel.projectListViewModel[index]
                height: 50

                width: gridView.cellWidth - 2 //root.width/grid.columns-grid.spacing;
                isSelected: (root.selectedId === index) ? true : false
                isNormal: (root.selectedId === -1) ? true : false
                //onClicked: askOpenDetail(index);//root.selectedId = index
                //onDoubleClicked: root.selectedId = index
            }
            Connections {
                target: projectView
                onClicked: {
                    if (root.selectedId === -1) {
                        console.debug(index)
                        root.selectedId = -1
                        askOpenDetail(
                                    projectListViewModel.projectListViewModel[index].id)
                    } else {
                        root.selectedId = index
                    }
                }
            }
            Connections {
                target: projectView
                onDoubleClicked: {
                    if (root.selectedId === -1)
                        root.selectedId = index
                    else
                        root.selectedId = -1
                }
            }
        }
    }
}




/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
