import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQml.Models 2.12
import core.backend.viewmodel 1.0
import "../qmlControls"

Page {
    id: root
    width: 400
    height: 400

    title: questionViewModel.question

    property QuestionViewModel questionViewModel: QuestionViewModel {}
    property alias editMode: details.editMode
    signal close

    ObjectModel {
        id: itemModel
        LabelTextArea {
            id: lbltxtQuestion
            text: questionViewModel.question
            labelText: "Question"
            editMode: details.editMode
        }

        LabelTextArea {
            id: lbltxtAnswer
            text: questionViewModel.answer
            labelText: qsTr("Réponse")
            editMode: details.editMode
        }

        LabelText {
            id: lbltxtWanted
            text: questionViewModel.wanted
            labelText: "Date souhaitée de réponse"
            height: 50
            inputMask: qsTr("99/99/9999")
            editMode: details.editMode
            //            validator: DateValidator {
            //            }
        }
    }
    DetailsPage {
        id: details
        model: itemModel
        anchors.fill: parent
    }

    Connections {
        target: lbltxtQuestion
        onTextChanged: questionViewModel.question = lbltxtQuestion.text;
    }



}
