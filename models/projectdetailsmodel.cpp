#include "projectdetailsmodel.h"

ProjectDetailsModel::ProjectDetailsModel(QObject *parent) : QObject(parent)
{
    m_ProjectModel      = new class ProjectModel(this);
    m_RiskListModel     = new class RiskListModel(this);
    m_QuestionListModel = new class QuestionListModel(this);
    m_TaskListModel     = new class TaskListModel(this);
}

bool ProjectDetailsModel::load(long i)
{
    if(i!=-1)
    {
        this->m_ProjectModel->load(i);
        this->m_RiskListModel->load(i);
        this->m_QuestionListModel->load(i);
        this->m_TaskListModel->load(i);
    }
    else
    {
        //this->m_ProjectModel->load();
        this->m_RiskListModel->load();
        this->m_QuestionListModel->load();
        this->m_TaskListModel->load();
    }
    return true;
}

bool ProjectDetailsModel::store()
{
    qCritical("store() method in ContactListModel not implemented.");
    return false;
}
