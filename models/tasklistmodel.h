#ifndef TASKLISTMODEL_H
#define TASKLISTMODEL_H

#include <QObject>
#include <QList>
#include "taskmodel.h"

#include "services/dbcontext.h"

class TaskListModel : public QObject
{
    Q_OBJECT
public:
    explicit TaskListModel(QObject *parent = nullptr);

    QList<TaskService::ShrPtr> tasksList() const
    {
        return m_lstTasks;
    }

    bool load(long i = -1);
    bool store();
signals:

public slots:

private:

    QList<TaskService::ShrPtr> m_lstTasks;
};

#endif // TASKLISTMODEL_H
