#include "incidentmodel.h"

IncidentModel::IncidentModel(QObject *parent) : QObject(parent)
{

}

IncidentModel::IncidentModel(IncidentService::ShrPtr incident, QObject *parent)
    :QObject (parent)
    ,m_incident(incident)
{

}

bool IncidentModel::load(long i)
{
    IncidentService::ShrPtr incident = DbContext::get().Incidents().get(i);
    if (incident.isNull())
        return false;
    m_incident = incident;
    return true;
}

bool IncidentModel::store()
{
    if(m_incident.isNull())
        return false;

    return DbContext::get().Incidents().store(m_incident);
}
