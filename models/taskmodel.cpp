#include "taskmodel.h"

TaskModel::TaskModel(QObject *parent) : QObject(parent)
{

}

TaskModel::TaskModel(TaskService::ShrPtr task, QObject *parent)
    : QObject(parent)
    , m_task( task )
{

}

bool TaskModel::load(long i)
{
    TaskService::ShrPtr task = DbContext::get().Tasks().get(i);
    if (task.isNull())
        return false;
    m_task = task;
    return true;
}

bool TaskModel::store()
{
    if(m_task.isNull())
        return false;

    return DbContext::get().Tasks().store(m_task);
}
