#ifndef PROJECTLISTMODEL_H
#define PROJECTLISTMODEL_H

#include <QObject>
#include <QList>
#include "projectmodel.h"

#include "services/dbcontext.h"

class ProjectListModel : public QObject
{
    Q_OBJECT
public:
    explicit ProjectListModel(QObject *parent = nullptr);

    QList<ProjectService::ShrPtr> projectsList() const
    {
        return m_lstProjects;
    }
    
    bool load(long i = -1);
    bool store();
signals:

public slots:
    
private:
    
    QList<ProjectService::ShrPtr> m_lstProjects;
};

#endif // PROJECTLISTMODEL_H
