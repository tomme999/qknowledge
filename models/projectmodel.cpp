#include "projectmodel.h"

ProjectModel::ProjectModel(QObject *parent)
    : QObject(parent)
    , m_project( DbContext::get().Projects().getNew() )
{

}

ProjectModel::ProjectModel(ProjectService::ShrPtr project, QObject *parent)
    : QObject(parent)
    , m_project( project )
{

}

bool ProjectModel::load(long i)
{
    ProjectService::ShrPtr project = DbContext::get().Projects().get(i);
    if (project.isNull())
        return false;
    m_project = project;
    return true;
}

bool ProjectModel::store()
{
    if(m_project.isNull())
        return false;

    return DbContext::get().Projects().store(m_project);
}
