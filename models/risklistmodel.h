#ifndef RISKLISTMODEL_H
#define RISKLISTMODEL_H

#include <QObject>
#include <QList>
#include "riskmodel.h"

#include "services/dbcontext.h"

class RiskListModel : public QObject
{
    Q_OBJECT
public:
    explicit RiskListModel(QObject *parent = nullptr);

    QList<RiskService::ShrPtr> risksList() const
    {
        return m_lstRisks;
    }

    bool load(long i = -1);
    bool store();
signals:

public slots:

private:

    QList<RiskService::ShrPtr> m_lstRisks;
};

#endif // RISKLISTMODEL_H
