#ifndef QUESTIONLISTMODEL_H
#define QUESTIONLISTMODEL_H

#include <QObject>
#include <QList>
#include "questionmodel.h"

#include "services/dbcontext.h"

class QuestionListModel : public QObject
{
    Q_OBJECT
public:
    explicit QuestionListModel(QObject *parent = nullptr);

    QList<QuestionService::ShrPtr> questionsList() const
    {
        return m_lstQuestions;
    }

    bool load(long i = -1);
    bool store();
signals:

public slots:

private :
    QList<QuestionService::ShrPtr> m_lstQuestions;
};

#endif // QUESTIONLISTMODEL_H
