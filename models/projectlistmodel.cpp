#include "projectlistmodel.h"

ProjectListModel::ProjectListModel(QObject *parent) : QObject(parent)
{
    
}

bool ProjectListModel::load(long i)
{
    Q_UNUSED(i);
    
    auto lst = DbContext::get().Projects().get();
    if(lst.count()==0)
    {
        return false;
    }
    else
    {
        this->m_lstProjects = lst;
        return true;
    }
}
