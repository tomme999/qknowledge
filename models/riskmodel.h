#ifndef RISKMODEL_H
#define RISKMODEL_H

#include <QObject>
#include "services/dbcontext.h"

class RiskModel : public QObject
{
    Q_OBJECT

    RiskService::ShrPtr m_risk;
public:
    explicit RiskModel(QObject *parent = nullptr);
    explicit RiskModel(RiskService::ShrPtr risk, QObject *parent = nullptr);

    RiskService::ShrPtr risk() const
    {
        return m_risk;
    }

    bool load(long i);
    bool store();
signals:

public slots:
};

#endif // RISKMODEL_H
