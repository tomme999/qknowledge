#ifndef CONTACTMODEL_H
#define CONTACTMODEL_H

#include <QObject>
#include "services/dbcontext.h"

class ContactModel : public QObject
{
    Q_OBJECT

public:
    explicit ContactModel(QObject *parent = nullptr);
    explicit ContactModel(ContactService::ShrPtr contact, QObject *parent = nullptr);

    ContactService::ShrPtr contact() const
    {
        return m_contact;
    }

    bool load(long i);
    bool store();

public slots:

private:

ContactService::ShrPtr m_contact;
};

#endif // CONTACTMODEL_H
