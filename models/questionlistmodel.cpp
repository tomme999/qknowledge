#include "questionlistmodel.h"

QuestionListModel::QuestionListModel(QObject *parent) : QObject(parent)
{

}

bool QuestionListModel::load(long i)
{
    Q_UNUSED(i);

    QList<QuestionService::ShrPtr> lst;
    if (i==-1)
        lst = DbContext::get().Questions().get();
    else
        lst = DbContext::get().Questions().getByIdProject(i);

    if(lst.count()==0)
    {
        return false;
    }
    else
    {
        this->m_lstQuestions = lst;
        return true;
    }
}
