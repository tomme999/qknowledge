#include "contactlistmodel.h"

ContactListModel::ContactListModel(QObject *parent) : QObject(parent)
{

}

bool ContactListModel::load(long i)
{
    if(i!=-1)
    {
        qCritical("load() method in ContactListModel not implemented.");
        return false;
    }
    else
    {
        QList<ContactService::ShrPtr> lst = DbContext::get().Contacts().get();
        foreach(ContactService::ShrPtr contact, lst)
        {
            this->m_lstContactModel << new ContactModel(contact, this);
        }
    }
    return true;
}

bool ContactListModel::store()
{
    qCritical("store() method in ContactListModel not implemented.");
    return false;
}
