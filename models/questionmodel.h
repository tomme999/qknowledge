#ifndef QUESTIONMODEL_H
#define QUESTIONMODEL_H

#include <QObject>
#include "services/dbcontext.h"

class QuestionModel : public QObject
{
    Q_OBJECT
    QuestionService::ShrPtr m_question;
public:
    explicit QuestionModel(QObject *parent = nullptr);
    explicit QuestionModel(QuestionService::ShrPtr question, QObject *parent = nullptr);

    QuestionService::ShrPtr question() const
    {
        return m_question;
    }

    bool load(long i);
    bool store();
signals:

public slots:
};

#endif // QUESTIONMODEL_H
