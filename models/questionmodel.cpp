#include "questionmodel.h"

QuestionModel::QuestionModel(QObject *parent) : QObject(parent)
{

}

QuestionModel::QuestionModel(QuestionService::ShrPtr question, QObject *parent)
    : QObject(parent)
    , m_question( question )
{

}

bool QuestionModel::load(long i)
{
    QuestionService::ShrPtr question = DbContext::get().Questions().get(i);
    if (question.isNull())
        return false;
    m_question = question;
    return true;
}

bool QuestionModel::store()
{
    if(m_question.isNull())
        return false;

    return DbContext::get().Questions().store(m_question);
}
