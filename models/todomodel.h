#ifndef TODOMODEL_H
#define TODOMODEL_H

#include <QObject>
#include "services/dbcontext.h"

class TodoModel: public QObject
{
public:
    TodoModel();

    Q_OBJECT

    TodoService::ShrPtr m_todo;
public:
    explicit TodoModel(QObject *parent = nullptr);
    explicit TodoModel(TodoService::ShrPtr project, QObject *parent = nullptr);

    TodoService::ShrPtr todo() const
    {
        return m_todo;
    }

    bool load(long i);
    bool store();
signals:

public slots:
};

#endif // TODOMODEL_H
