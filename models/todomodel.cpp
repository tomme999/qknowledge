#include "todomodel.h"


TodoModel::TodoModel(QObject *parent) : QObject(parent)
{

}

TodoModel::TodoModel(TodoService::ShrPtr todo, QObject *parent)
    : QObject(parent)
    , m_todo( todo )
{

}

bool TodoModel::load(long i)
{
    TodoService::ShrPtr todo = DbContext::get().Todos().get(i);
    if (todo.isNull())
        return false;
    m_todo = todo;
    return true;
}

bool TodoModel::store()
{
    if(m_todo.isNull())
        return false;

    return DbContext::get().Todos().store(m_todo);
}
