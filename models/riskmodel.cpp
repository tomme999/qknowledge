#include "riskmodel.h"

RiskModel::RiskModel(QObject *parent) : QObject(parent)
{

}

RiskModel::RiskModel(RiskService::ShrPtr risk, QObject *parent)
    : QObject(parent)
    , m_risk( risk )
{

}

bool RiskModel::load(long i)
{
    RiskService::ShrPtr risk = DbContext::get().Risks().get(i);
    if (risk.isNull())
        return false;
    m_risk = risk;
    return true;
}

bool RiskModel::store()
{
    if(m_risk.isNull())
        return false;

    return DbContext::get().Risks().store(m_risk);
}
