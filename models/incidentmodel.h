#ifndef INCIDENTMODEL_H
#define INCIDENTMODEL_H

#include <QObject>
#include "services/dbcontext.h"

class IncidentModel : public QObject
{
    Q_OBJECT
    IncidentService::ShrPtr m_incident;

public:
    explicit IncidentModel(QObject *parent = nullptr);
    explicit IncidentModel(IncidentService::ShrPtr incident, QObject *parent = nullptr);

    IncidentService::ShrPtr incident() {    return m_incident;  }

    bool load(long i);
    bool store();
signals:

public slots:
};

#endif // INCIDENTMODEL_H
