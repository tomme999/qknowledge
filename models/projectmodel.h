#ifndef PROJECTMODEL_H
#define PROJECTMODEL_H

#include <QObject>
#include "services/dbcontext.h"

class ProjectModel : public QObject
{
    Q_OBJECT
public:
    explicit ProjectModel(QObject *parent = nullptr);
    explicit ProjectModel(ProjectService::ShrPtr project, QObject *parent = nullptr);

    ProjectService::ShrPtr project() const
    {
        return m_project;
    }

    bool load(long i);
    bool store();
signals:

public slots:

private:

ProjectService::ShrPtr m_project;
};

#endif // PROJECTMODEL_H
