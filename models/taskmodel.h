#ifndef TASKMODEL_H
#define TASKMODEL_H

#include <QObject>
#include "services/dbcontext.h"

class TaskModel : public QObject
{
    Q_OBJECT

    TaskService::ShrPtr m_task;
public:
    explicit TaskModel(QObject *parent = nullptr);
    explicit TaskModel(TaskService::ShrPtr project, QObject *parent = nullptr);

    TaskService::ShrPtr task() const
    {
        return m_task;
    }

    bool load(long i);
    bool store();
signals:

public slots:
};

#endif // TASKMODEL_H
