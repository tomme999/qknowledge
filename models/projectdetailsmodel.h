#ifndef PROJECTDETAILSMODEL_H
#define PROJECTDETAILSMODEL_H

#include <QObject>
#include <models/projectmodel.h>
#include <models/questionlistmodel.h>
#include <models/risklistmodel.h>
#include <models/tasklistmodel.h>

class ProjectDetailsModel : public QObject
{
    Q_OBJECT

    ProjectModel        * m_ProjectModel;
    QuestionListModel   * m_QuestionListModel;
    RiskListModel       * m_RiskListModel;
    TaskListModel       * m_TaskListModel;

public:
    explicit ProjectDetailsModel(QObject *parent = nullptr);

    bool load(long i = -1);
    bool store();

    ProjectModel        *projectModel()      const  {   return this->m_ProjectModel;        }
    QuestionListModel   *questionListModel() const  {   return this->m_QuestionListModel;   }
    RiskListModel       *riskListModel()     const  {   return this->m_RiskListModel;       }
    TaskListModel       *taskListModel()     const  {   return this->m_TaskListModel;       }

signals:

public slots:
};

#endif // PROJECTDETAILSMODEL_H
