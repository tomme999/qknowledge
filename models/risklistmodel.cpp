#include "risklistmodel.h"

RiskListModel::RiskListModel(QObject *parent) : QObject(parent)
{

}

bool RiskListModel::load(long i)
{

    this->m_lstRisks.clear();
    auto lst = (i==-1)
                ? DbContext::get().Risks().get()
                : DbContext::get().Risks().getByIdProject(i);
    if(lst.count()==0)
    {
        return false;
    }
    else
    {
        this->m_lstRisks.append(lst);
        return true;
    }
}
