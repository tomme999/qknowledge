#include "contactmodel.h"

ContactModel::ContactModel(QObject *parent) : QObject(parent),  m_contact(DbContext::get().Contacts().getNew())
{

}

ContactModel::ContactModel(ContactService::ShrPtr contact, QObject *parent):
    QObject(parent),
    m_contact(contact)
{

}

bool ContactModel::load(long i)
{
    ContactService::ShrPtr contact = DbContext::get().Contacts().get(i);
    if (contact.isNull())
        return false;
    m_contact = contact;
    return true;
}

bool ContactModel::store()
{
    if(m_contact.isNull())
        return false;

    return DbContext::get().Contacts().store(m_contact);
}
