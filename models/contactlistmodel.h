#ifndef CONTACTLISTMODEL_H
#define CONTACTLISTMODEL_H

#include <QList>
#include <QObject>
#include <models/contactmodel.h>
#include <services/dbcontext.h>

class ContactListModel : public QObject
{
    Q_OBJECT

    QList<ContactService::ShrPtr> m_lstContact;
    QList<ContactModel *> m_lstContactModel;
public:
    explicit ContactListModel(QObject *parent = nullptr);

    QList<ContactModel *> contactModels() { return m_lstContactModel; }

    bool load(long i = -1);
    bool store();

signals:

public slots:
};
#endif // CONTACTLISTMODEL_H
