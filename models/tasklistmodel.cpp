#include "tasklistmodel.h"

TaskListModel::TaskListModel(QObject *parent) : QObject(parent)
{

}

bool TaskListModel::load(long i)
{

    auto lst = (i==-1)
            ? DbContext::get().Tasks().get()
            : DbContext::get().Tasks().getByIdProject(i);

    if(lst.count()==0)
    {
        return false;
    }
    else
    {
        this->m_lstTasks = lst;
        return true;
    }
}
