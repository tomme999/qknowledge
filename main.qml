import QtQuick 2.9
import QtQuick.Controls 2.2
import QtGraphicalEffects 1.0
import QtQuick.Dialogs 1.1
import "views"
import core.backend.viewmodel 1.0

ApplicationWindow {
    id: window
    visible: true
    width: 640
    height: 480

    title: qsTr("Knowlege")

    header: ToolBar {
        contentHeight: toolButton.implicitHeight
        ToolButton {
            id: toolButton
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                if (stackView.depth > 1) {
                    stackView.pop()
                } else {
                    drawer.open()
                }
            }
        }

        Label {
            text: stackView.currentItem.title
            anchors.centerIn: parent
        }

        ToolButton {
            id: toolButtonAdd
            icon.source: "qrc:/assets/icons/add.svg"
            anchors.right: parent.right
        }
    }

    Drawer {
        id: drawer
        //width: window.width * 0.66
        width: 200;
        height: window.height

        Column {
            anchors.fill: parent

            Image {
                id: imglogo;
                source: "qrc:/assets/BrainLogo.jpg";


                Label {
                    id: lblAppName;
                    text: "Knowledge";
                    anchors.centerIn: parent;
                    font.pixelSize: 20;
                    color: "#ffffff"
                }
                Glow {
                    anchors.fill: lblAppName
                    radius: 8
                    samples: 17
                    color: "black"
                    source: lblAppName
                }
            }

            ItemDelegate {
                text: qsTr("Projets")
                width: parent.width
                icon {
                    source:  "qrc:/assets/icons/project-dark.svg";
                    height: 25;
                    width: 25;
                }
                onClicked: {
                    stackView.push("qrc:/views/ProjectListViewForm.ui.qml")
                    //stackView.push({item: "qrc:/views/ProjectListViewForm.ui.qml", properties:{anchors:{fill:parent}}})

                    stackView.currentItem.width = stackView.width;
                    stackView.currentItem.width = Qt.binding(function() { return stackView.width });
                    stackView.currentItem.height = stackView.height;
                    stackView.currentItem.height = Qt.binding(function() { return stackView.height });

                    //stackView.currentItem.askOpenDetail.connect(function(idProjet) { console.debug("Youpi"+idProjet); })
                    stackView.currentItem.askOpenDetail.connect(function(idProject) {openProjectDetails(idProject);});

                    stackView.currentItem.projectListViewModel.load();
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Contacts")
                width: parent.width
                icon {
                    source:  "qrc:/assets/icons/contacts-dark.svg";
                    height: 25;
                    width: 25;
                }
                onClicked: {
                    stackView.push("qrc:/views/ContactListViewForm.ui.qml")
                    stackView.currentItem.width = stackView.width;
                    stackView.currentItem.width = Qt.binding(function() { return stackView.width });
                    stackView.currentItem.height = stackView.height;
                    stackView.currentItem.height = Qt.binding(function() { return stackView.height });

                    stackView.currentItem.contactListViewModel.load();
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Alertes")
                width: parent.width
                icon {
                    source:  "qrc:/assets/icons/events.svg";
                    height: 25;
                    width: 25;
                }
                onClicked: {
                    stackView.push("Page1Form.ui.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Statistiques")
                width: parent.width
                icon {
                    source:  "qrc:/assets/icons/stats-dark.svg";
                    height: 25;
                    width: 25;
                }
                onClicked: {
                    stackView.push("Page2Form.ui.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Connaissances")
                width: parent.width
                icon {
                    source:  "qrc:/assets/icons/knowledge.svg";
                    height: 25;
                    width: 25;
                }
                onClicked: {
                    stackView.push("qrc:/views/ProjectDetailsViewForm.ui.qml")
                    stackView.currentItem.width = stackView.width;
                    stackView.currentItem.width = Qt.binding(function() { return stackView.width });
                    stackView.currentItem.height = stackView.height;
                    stackView.currentItem.height = Qt.binding(function() { return stackView.height });
                    stackView.currentItem.projectDetailViewModel.load();
                    drawer.close()
                }
            }
        }
    }

    StackView {
        id: stackView
        initialItem: "HomeForm.ui.qml"
        anchors.fill: parent
    }

   /* Shortcut {
        sequence: StandardKey.Backspace;
        onActivated: pop();
    }*/

    MessageDialog {
        id: messageDialog
        title: "May I have your attention please"
        text: "It's so cool that you are using Qt Quick."
        onAccepted: {
            console.log("And of course you could only agree.")
            Qt.quit()
        }
        //Component.onCompleted: visible = true
    }

    onClosing: {
        if(stackView.depth>1)
        {
            close.accepted = false;
            stackView.pop();
        }

        close.accepted = true;
    }
//    Connections {
//        target: mainViewModel
//        onAskOpen: {
//            console.debug("Ask Open !");
//            switch(viewEnum)
//            {
//            case MainViewModel.ERiskView:
//                openView("qrc:/views/TaskView.qml");

//                stackView.currentItem.taskViewModel = viewModel;
//                break;
//            default:
//                console.debug("zut alors " + View)
//            }
//        }
//    }
    Connections {
        target: mainViewModel
        onOpenRiskEditAsked: {
            openView("qrc:/views/RiskView.qml");
            stackView.currentItem.taskViewModel = vm;
        }
    }
    Connections {
        target: mainViewModel
        onOpenQuestionEditAsked: {
            openView("qrc:/views/QuestionEditView.qml");
            stackView.currentItem.questionViewModel = vm;
            stackView.currentItem.editMode = false;
        }
    }

    function openProjectDetails(idProject)
    {
        console.debug("idproject ici "+idProject)
        stackView.push("qrc:/views/ProjectDetailsViewForm.ui.qml")
        stackView.currentItem.width = stackView.width;
        stackView.currentItem.width = Qt.binding(function() { return stackView.width });
        stackView.currentItem.height = stackView.height;
        stackView.currentItem.height = Qt.binding(function() { return stackView.height });
        stackView.currentItem.projectDetailViewModel.load(idProject);
    }

    function openView(viewName)
    {
        stackView.push(viewName)
        stackView.currentItem.width = stackView.width;
        stackView.currentItem.width = Qt.binding(function() { return stackView.width });
        stackView.currentItem.height = stackView.height;
        stackView.currentItem.height = Qt.binding(function() { return stackView.height });
    }
}
