#include "task.h"

Task::Task(QObject *parent) : BusinessProject(parent)
{

}

Task::Task(QString name, QString description, QDate endDateWanted, long idRoleOwner, long idProject, QObject *parent) :
    BusinessProject  (  parent         ,    idProject),
    m_name           (  name           ),
    m_description    (  description    ),
    m_endDateWanted  (  endDateWanted  ),
    m_idRoleOwner    (  idRoleOwner    )
{

}
