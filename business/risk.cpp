#include "risk.h"

Risk::Risk(QObject *parent) : BusinessProject (parent)
{

}

Risk::Risk(QDate startDate, QString name, QString description, int level, int probability, long idContactOwner, QString actionToLead, bool isClosed, QDate endDateWanted, QObject *parent):
    BusinessProject   ( parent          ),
    m_startDate       ( startDate       ),
    m_name            ( name            ),
    m_description     ( description     ),
    m_level           ( level           ),
    m_probability     ( probability     ),
    m_idContactOwner  ( idContactOwner  ),
    m_actionToLead    ( actionToLead    ),
    m_isClosed        ( isClosed        ),
    m_endDateWanted   ( endDateWanted   )
{

}
