#ifndef TASK_H
#define TASK_H

#include "ext/businessproject.h"
#include "rules/taskrules.h"

class Task : public BusinessProject
{
    Q_OBJECT

    Q_PROPERTY(QDate        endDateWanted   READ    endDateWanted   WRITE setEndDateWanted  NOTIFY endDateWantedChanged     )
    Q_PROPERTY(QString      description     READ    description     WRITE setDescription    NOTIFY descriptionChanged       )
    Q_PROPERTY(QString      name            READ    name            WRITE setName           NOTIFY nameChanged              )
    Q_PROPERTY(long         idRoleOwner     READ    idRoleOwner     WRITE setIdRoleOwner    NOTIFY idRoleOwnerChanged       )

    QString                 m_name          ;
    QString                 m_description   ;
    QDate                   m_endDateWanted ;
    long                    m_idRoleOwner   ;

public:
    explicit Task(QObject *parent = nullptr);
    explicit Task(QString name, QString description, QDate endDateWanted, long idRoleOwner, long idProject, QObject *parent = nullptr);

    QDate   endDateWanted () const      {   return  m_endDateWanted ;       }
    QString description   () const      {   return  m_description   ;       }
    QString name          () const      {   return  m_name          ;       }
    long    idRoleOwner   () const      {   return  m_idRoleOwner   ;       }

signals:

    void    descriptionChanged          (    QString     description        );
    void    endDateWantedChanged        (    QDate       endDateWanted      );
    void    idRoleOwnerChanged          (    long        idRoleOwner        );
    void    nameChanged                 (    QString     name               );

public slots:

    void    setDescription              (    QString     description        )    _VSETTER__( TaskRules,  description        )
    void    setEndDateWanted            (    QDate       endDateWanted      )    _VSETTER__( TaskRules,  endDateWanted      )
    void    setIdRoleOwner              (    long        idRoleOwner        )    _VSETTER__( TaskRules,  idRoleOwner        )
    void    setName                     (    QString     name               )    _VSETTER__( TaskRules,  name               )

};

#endif // TASK_H
