#ifndef TODO_H
#define TODO_H

#include "ext/businessproject.h"
#include "rules/todorules.h"

class Todo : public BusinessProject
{
    Q_OBJECT

    Q_PROPERTY(QDate        endDateWanted   READ    endDateWanted   WRITE setEndDateWanted  NOTIFY endDateWantedChanged     )
    Q_PROPERTY(QString      description     READ    description     WRITE setDescription    NOTIFY descriptionChanged       )
    Q_PROPERTY(QString      name            READ    name            WRITE setName           NOTIFY nameChanged              )
    Q_PROPERTY(long         idRoleOwner     READ    idRoleOwner     WRITE setIdRoleOwner    NOTIFY idRoleOwnerChanged       )
    Q_PROPERTY(bool         done            READ    done            WRITE setDone           NOTIFY doneChanged              )

    QString                 m_name          ;
    QString                 m_description   ;
    QDate                   m_endDateWanted ;
    long                    m_idRoleOwner   ;
    bool                    m_done          ;

public:
    explicit Todo(QObject *parent = nullptr);
    explicit Todo(QString name, QString description, QDate endDateWanted, long idRoleOwner, long idProject, QObject *parent = nullptr);

    QDate   endDateWanted () const      {   return  m_endDateWanted ;       }
    QString description   () const      {   return  m_description   ;       }
    QString name          () const      {   return  m_name          ;       }
    long    idRoleOwner   () const      {   return  m_idRoleOwner   ;       }
    bool    done          () const      {   return  m_done          ;       }

signals:

    void    descriptionChanged          (    QString     description        );
    void    endDateWantedChanged        (    QDate       endDateWanted      );
    void    idRoleOwnerChanged          (    long        idRoleOwner        );
    void    nameChanged                 (    QString     name               );
    void    doneChanged                 (    bool        done               );

public slots:

    void    setDescription              (    QString     description        )    _VSETTER__( TodoRules,  description        )
    void    setEndDateWanted            (    QDate       endDateWanted      )    _VSETTER__( TodoRules,  endDateWanted      )
    void    setIdRoleOwner              (    long        idRoleOwner        )    _VSETTER__( TodoRules,  idRoleOwner        )
    void    setName                     (    QString     name               )    _VSETTER__( TodoRules,  name               )
    void    setDone                     (    bool        done               )    _VSETTER__( TodoRules,  done               )

};

#endif // TODO_H
