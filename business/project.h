#ifndef PROJECT_H
#define PROJECT_H

#include "../core/business.h"
#include "rules/projectrules.h"

class Project : public Business
{
    Q_OBJECT
    Q_PROPERTY(QString  description     READ description    WRITE setDescription    NOTIFY descriptionChanged)
    Q_PROPERTY(QString  name            READ name           WRITE setName           NOTIFY nameChanged)

    QString m_name;
    QString m_description;
public:

    explicit Project(QObject *parent = nullptr);

    QString name()          const   {    return m_name;         }
    QString description()   const   {    return m_description;  }

signals:

    void nameChanged                (   QString name            );
    void descriptionChanged         (   QString description     );

public slots:

    void setName                    (   QString name            )    _VSETTER__(    ProjectRules,   name            )
    void setDescription             (   QString description     )    _VSETTER__(    ProjectRules,   description     )

};


#endif // PROJECT_H
