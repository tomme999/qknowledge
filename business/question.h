#ifndef QUESTION_H
#define QUESTION_H

#include <QDate>
#include <QObject>
#include <QString>

#include "ext/businessproject.h"
#include "rules/questionrules.h"

class Question : public BusinessProject
{
    Q_OBJECT
    Q_PROPERTY(QDate    answerDate      READ answerDate     WRITE setAnswerDate     NOTIFY answerDateChanged)   // Date de la reponse
    Q_PROPERTY(QDate    wanted          READ wanted         WRITE setWanted         NOTIFY wantedChanged)       // Date souhaite pour la reponse
    Q_PROPERTY(QString  answer          READ answer         WRITE setAnswer         NOTIFY answerChanged)       // Reponse de la question
    Q_PROPERTY(QString  question        READ question       WRITE setQuestion       NOTIFY questionChanged)     // Intitule de la question

    QString m_question;
    QString m_answer;
    QDate   m_answerDate;
    QDate   m_wanted;

public:

    explicit Question(QObject *parent = nullptr);

    QString question()      const   {   return m_question;      }
    QString answer()        const   {   return m_answer;        }
    QDate   answerDate()    const   {   return m_answerDate;    }
    QDate   wanted()        const   {   return m_wanted;        }

signals:

    void questionChanged            (   QString question        );
    void answerChanged              (   QString answer          );
    void answerDateChanged          (   QDate answerDate        );
    void wantedChanged              (   QDate wanted            );

public slots:

    void setQuestion                (   QString question        )    _VSETTER__(   QuestionRules,   question        )
    void setAnswer                  (   QString answer          )    _VSETTER__(   QuestionRules,   answer          )
    void setAnswerDate              (   QDate answerDate        )    _VSETTER__(   QuestionRules,   answerDate      )
    void setWanted                  (   QDate wanted            )    _VSETTER__(   QuestionRules,   wanted          )
};

#endif // QUESTION_H
