#ifndef RISK_H
#define RISK_H

#include "ext/businessproject.h"
#include "rules/riskrules.h"

class Risk : public BusinessProject
{
public:
    enum    eLevel          {   LevelNone = 0,          LevelLow = 1,           LevelMedium = 2,            LevelHigh = 3       };
    enum    eProbability    {   ProbabilityNone = 0,    ProbabilityLow = 1,     ProbabilityMedium = 2,      ProbabilityHigh = 3 };


private:
    Q_OBJECT

    Q_ENUMS   (    eLevel          )
    Q_ENUMS   (    eProbability    )

    Q_PROPERTY(QDate        endDateWanted   READ    endDateWanted   WRITE setEndDateWanted  NOTIFY endDateWantedChanged     )
    Q_PROPERTY(QDate        startDate       READ    startDate       WRITE setStartTime      NOTIFY startDateChanged         )
    Q_PROPERTY(QString      actionToLead    READ    actionToLead    WRITE setActionToLead   NOTIFY actionToLeadChanged      )
    Q_PROPERTY(QString      description     READ    description     WRITE setDescription    NOTIFY descriptionChanged       )
    Q_PROPERTY(QString      name            READ    name            WRITE setName           NOTIFY nameChanged              )
    Q_PROPERTY(bool         isClosed        READ    isClosed        WRITE setIsClosed       NOTIFY isClosedChanged          )
    Q_PROPERTY(int          level           READ    level           WRITE setLevel          NOTIFY levelChanged             )
    Q_PROPERTY(int          probability     READ    probability     WRITE setProbability    NOTIFY probabilityChanged       )
    Q_PROPERTY(long         idContactOwner  READ    idContactOwner  WRITE setIdContactOwner NOTIFY idContactOwnerChanged    )

    QDate           m_startDate       ;
    QString         m_name            ;
    QString         m_description     ;
    int             m_level           ;
    int             m_probability     ;
    long            m_idContactOwner  ;
    QString         m_actionToLead    ;
    bool            m_isClosed        ;
    QDate           m_endDateWanted   ;

public:

    explicit Risk(QObject *parent = nullptr);
    explicit Risk(
        QDate          startDate       ,
        QString        name            ,
        QString        description     ,
        int            level           ,
        int            probability     ,
        long           idContactOwner  ,
        QString        actionToLead    ,
        bool           isClosed        ,
        QDate          endDateWanted   ,
        QObject *parent = nullptr       );

    QDate           endDateWanted  () const {    return m_endDateWanted  ; }
    QDate           startDate      () const {    return m_startDate      ; }
    QString         actionToLead   () const {    return m_actionToLead   ; }
    QString         description    () const {    return m_description    ; }
    QString         name           () const {    return m_name           ; }
    bool            isClosed       () const {    return m_isClosed       ; }
    int             level          () const {    return m_level          ; }
    int             probability    () const {    return m_probability    ; }
    long            idContactOwner () const {    return m_idContactOwner ; }

signals:

    void    actionToLeadChanged         (     QString       actionToLead      );
    void    descriptionChanged          (     QString       description       );
    void    endDateWantedChanged        (     QDate         endDateWanted     );
    void    idContactOwnerChanged       (     long          idContactOwner    );
    void    isClosedChanged             (     bool          isClosed          );
    void    levelChanged                (     int           level             );
    void    nameChanged                 (     QString       name              );
    void    probabilityChanged          (     int           probability       );
    void    startDateChanged            (     QDate         startDate         );

public slots:

    void    setActionToLead             (     QString       actionToLead      )   _VSETTER__( RiskRules,       actionToLead      )
    void    setDescription              (     QString       description       )   _VSETTER__( RiskRules,       description       )
    void    setEndDateWanted            (     QDate         endDateWanted     )   __SETTER__( endDateWanted     )
    void    setIdContactOwner           (     long          idContactOwner    )   _VSETTER__( RiskRules,       idContactOwner    )
    void    setIsClosed                 (     bool          isClosed          )   __SETTER__( isClosed          )
    void    setLevel                    (     int           level             )   _VSETTER__( RiskRules,       level             )
    void    setName                     (     QString       name              )   _VSETTER__( RiskRules,       name              )
    void    setProbability              (     int           probability       )   _VSETTER__( RiskRules,       probability       )
    void    setStartTime                (     QDate         startDate         )   _VSETTER__( RiskRules,       startDate         )

};
Q_DECLARE_METATYPE(Risk::eLevel)
Q_DECLARE_METATYPE(Risk::eProbability)
#endif // RISK_H
