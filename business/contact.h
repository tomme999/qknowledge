#ifndef CONTACT_H
#define CONTACT_H

#include "../core/business.h"
#include "rules/contactrules.h"

//using namespace ContactRules;

class Contact : public Business
{
    Q_OBJECT
    Q_PROPERTY(QString  email           READ email          WRITE setEmail          NOTIFY emailChanged)
    Q_PROPERTY(QString  firstName       READ firstName      WRITE setFirstName      NOTIFY firstNameChanged)
    Q_PROPERTY(QString  name            READ name           WRITE setName           NOTIFY nameChanged)
    Q_PROPERTY(QString  phoneNumber     READ phoneNumber    WRITE setPhoneNumber    NOTIFY phoneNumberChanged)

    QString m_name;
    QString m_firstName;
    QString m_phoneNumber;
    QString m_email;

public:

    explicit Contact(QObject *parent = nullptr);
    explicit Contact(QString name, QString firstName, QString phoneNumber, QString eMail, QObject *parent=  nullptr);

    QString name()          const   {    return m_name;         }
    QString firstName()     const   {    return m_firstName;    }
    QString phoneNumber()   const   {    return m_phoneNumber;  }
    QString email()         const   {    return m_email;        }

signals:

    void nameChanged                (   QString name            );
    void firstNameChanged           (   QString firstName       );
    void phoneNumberChanged         (   QString phoneNumber     );
    void emailChanged               (   QString email           );

public slots:

    void setName                    (   QString name            )    _VSETTER__(    ContactRules,   name            )
    void setFirstName               (   QString firstName       )    _VSETTER__(    ContactRules,   firstName       )
    void setPhoneNumber             (   QString phoneNumber     )    _VSETTER__(    ContactRules,   phoneNumber     )
    void setEmail                   (   QString email           )    _VSETTER__(    ContactRules,   email           )

};

#endif // CONTACT_H
