#ifndef BUSINESSPROJECT_H
#define BUSINESSPROJECT_H

#include <QObject>

#include "businessprojectrules.h"
#include <core/business.h>

class BusinessProject : public Business
{
    Q_OBJECT
    Q_PROPERTY(long         idProject       READ    idProject       WRITE setIdProject      NOTIFY idProjectChanged         )
    long                    m_idProject     ;

public:
    explicit BusinessProject(QObject *parent = nullptr,     long idProject = -1);

    long    idProject     () const      {   return  m_idProject     ;       }

    static QString getIdProjectPropertyName()
                                        {   return  "idProject"     ;       }
signals:
    void    idProjectChanged            (    long        idProject          );

public slots:
    void    setIdProject                (    long        idProject          )    _VSETTER__( BusinessProjectRules,  idProject          )

};

#endif // BUSINESSPROJECT_H
