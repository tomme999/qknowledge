#ifndef BUSINESSPROJECTRULES_H
#define BUSINESSPROJECTRULES_H

#include <core/businessrules.h>

namespace BusinessProjectRules {
    BUSINESS_VALIDATION_RULE(   long   ,    idProject,             return idProject>=-1           )
}
#endif // BUSINESSPROJECTRULES_H
