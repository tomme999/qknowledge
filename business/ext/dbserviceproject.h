#ifndef DBSERVICEPROJECT_H
#define DBSERVICEPROJECT_H

#include <QDebug>
#include <QList>
#include <QMetaProperty>
#include <QObject>
#include <QSharedPointer>
#include <QString>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>

#include <core/dbservice.h>
#include <core/dbservicehelper.h>

template <class BUSINESS> class DBServiceProject : public DBService<BUSINESS>
{
public:
    virtual typename DBService<BUSINESS>::LstShrPtr          getByIdProject
        (   long    idProject  )  const
    {
        typename DBService<BUSINESS>::LstShrPtr lst;

        QString Query           ( DBService<BUSINESS>::m_select                     );
        QString idFieldsName    ( BUSINESS::getIdProjectPropertyName()              );
        QString strId           ( DBServiceHelper::bindedVarName(idFieldsName  )    );
        QVariant lId            = QVariant::fromValue(idProject);

        QString lQuery = QString("%1 WHERE %2 = %3").arg(Query).arg(idFieldsName).arg(strId);

        qDebug() << lQuery;

        QSqlQuery qSqlQuery;

        if(!DBServiceHelper::prepareQuery(qSqlQuery, lQuery))
            return lst;

        qSqlQuery.bindValue(strId,lId);

        if(!qSqlQuery.exec())
            return lst;

        while(qSqlQuery.next())
        {
            typename DBService<BUSINESS>::ShrPtr p(new BUSINESS ());
            DBServiceHelper::populate( *p, qSqlQuery);
            lst.append(p);
        }
        return lst;
    }
};

#endif // DBSERVICEPROJECT_H
