#include "contact.h"

Contact::Contact(QObject *parent) : Business(parent)
{

}

Contact::Contact( QString name, QString firstName, QString phoneNumber, QString eMail, QObject *parent) :
    Business        (   parent      ),
    m_name          (   name        ),
    m_firstName     (   firstName   ),
    m_phoneNumber   (   phoneNumber ),
    m_email         (   eMail       )
{ }
