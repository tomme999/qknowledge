#ifndef QUESTIONRULES_H
#define QUESTIONRULES_H

#include <QDate>
#include <QString>

#include <core/businessrules.h>

namespace QuestionRules {
    BUSINESS_VALIDATION_RULE(   QDate,      answerDate,             return answerDate.isValid()         )
    BUSINESS_VALIDATION_RULE(   QDate,      wanted,                 return wanted.isValid()             )
    BUSINESS_VALIDATION_RULE(   QString,    answer,                 return answer!=""                   )
    BUSINESS_VALIDATION_RULE(   QString,    question,               return question!=""                 )
}
#endif // QUESTIONRULES_H
