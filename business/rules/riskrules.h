#ifndef RISKRULES_H
#define RISKRULES_H

#include <QDate>
#include <QString>

#include <core/businessrules.h>

namespace RiskRules {
    //BUSINESS_VALIDATION_RULE(   QDate,                               endDateWanted,    return true      )
    //BUSINESS_VALIDATION_RULE(   Risks::Level::eLevel,                level,            return level>0                  )
    //BUSINESS_VALIDATION_RULE(   Risks::Probability::eProbability,    probability,      return true      )
    //BUSINESS_VALIDATION_RULE(   bool,                                isClosed,         return true      )
    BUSINESS_VALIDATION_RULE(   QDate,                               startDate,        return !startDate.isNull()      )
    BUSINESS_VALIDATION_RULE(   QString,                             actionToLead,     return actionToLead!=""         )
    BUSINESS_VALIDATION_RULE(   QString,                             description,      return description!=""          )
    BUSINESS_VALIDATION_RULE(   QString,                             name,             return name!=""                 )
    BUSINESS_VALIDATION_RULE(   int,                                 level,             return level>=0 && level<=3    )
    BUSINESS_VALIDATION_RULE(   int,                                 probability,       return probability>=0 && probability<=3    )
    BUSINESS_VALIDATION_RULE(   long,                                idContactOwner,   return idContactOwner>0         )
}
#endif // RISKRULES_H
