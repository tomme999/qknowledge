#ifndef CONTACTRULES_H
#define CONTACTRULES_H

#include <QString>

#include <core/businessrules.h>

namespace ContactRules {
    BUSINESS_VALIDATION_RULE(   QString,    email,              return email!=""        )
    BUSINESS_VALIDATION_RULE(   QString,    firstName,          return firstName!=""    )
    BUSINESS_VALIDATION_RULE(   QString,    name,               return name!=""         )
    BUSINESS_VALIDATION_RULE(   QString,    phoneNumber,        return phoneNumber!=""  )
    BUSINESS_VALIDATION_RULE(   long   ,    id,                 return id>=-1           )

    inline QString constactInitials(QString const & firstName, QString const & name) { return QString("%0%1").arg(firstName[0]).arg(name[0]); }
}
#endif // CONTACTRULES_H
