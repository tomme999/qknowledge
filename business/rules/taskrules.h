#ifndef TASKRULES_H
#define TASKRULES_H

#include <QDate>
#include <QString>

#include <core/businessrules.h>

namespace TaskRules {
    BUSINESS_VALIDATION_RULE(   QDate,       endDateWanted,                   return endDateWanted.isValid()   )
    BUSINESS_VALIDATION_RULE(   QString,     description,                     return description!=""           )
    BUSINESS_VALIDATION_RULE(   QString,     name,                            return name!=""                  )
    BUSINESS_VALIDATION_RULE(   long   ,     id,                              return id>=-1                    )
    BUSINESS_VALIDATION_RULE(   long,        idRoleOwner,                     return idRoleOwner>0             )
}

#endif // TASKRULES_H
