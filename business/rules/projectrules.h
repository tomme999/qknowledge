#ifndef PROJECTRULES_H
#define PROJECTRULES_H

#include <QString>
#include <QStringList>

#include <core/businessrules.h>

namespace ProjectRules {
    BUSINESS_VALIDATION_RULE(   QString,    description,        return description!=""  )
    BUSINESS_VALIDATION_RULE(   QString,    name,               return name!=""         )
    BUSINESS_VALIDATION_RULE(   long   ,    id,                 return id>=-1           )

    inline QString projectInitials (   QString     const & projectName    )
    {
        QStringList lst = projectName.split(" ");
        QString returnValue;
        if(lst.count()>=2) returnValue = QString("%1%2").arg(lst.at(0)[0]).arg(lst.at(1)[0]);
        else returnValue = projectName.left(2);
        return returnValue;
    }
}
#endif // PROJECTRULES_H
