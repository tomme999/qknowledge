#include "todo.h"


Todo::Todo(QObject *parent) : BusinessProject(parent)
{

}

Todo::Todo(QString name, QString description, QDate endDateWanted, long idRoleOwner, long idProject, QObject *parent) :
    BusinessProject  (  parent         ,    idProject),
    m_name           (  name           ),
    m_description    (  description    ),
    m_endDateWanted  (  endDateWanted  ),
    m_idRoleOwner    (  idRoleOwner    )
{

}
