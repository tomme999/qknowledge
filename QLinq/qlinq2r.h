#ifndef QLINQ2R_H
#define QLINQ2R_H

#include "qlinq2q.h"
#include <algorithm>
#include <functional>
#include <iterator>
#include <QObject>
#include <QQueue>
#include <QStack>

#define Lbd2(Z,Y) [Z](auto arg0){ return Y; }

/* Lambda Returning Void with arg0 */
#define voidLbd(X) [](auto arg0) { X; }
/* Lambda Returning bool with arg0 */
#define retLbd(X) [](auto arg0) { return X; }

template <class T> class QLinq2R
{

    typedef typename T::value_type TI;
    typedef typename std::remove_pointer<TI>::type  TINP;

private :

public:
    QLinq2R(const T &myList, T(TINP::* const childreen)()) : _t(myList), _childreen(childreen) { }

    template <typename O>
    inline O avg(const std::function<O(TI)> &func) const
    {
        AvgTemporaty<O> avgTemporary { O(), 0 };
        this->_avg(this->_t, func, avgTemporary);

        O myReturn = avgTemporary.sum / avgTemporary.count;
        return myReturn;
    }

    template <typename O>
    inline O avg(O (TINP::* pm)() const) const
    {
        using namespace std::placeholders;
        //return this->avg<O>( [pm] (auto a) { return std::bind(pm, a)() ; } );
        //return this->avg<O>( std::bind(pm, std::placeholders::_1) );
        return this->avg<O>( _1 ->* pm );
    }

    //! \brief sums a property of TI
    //! \tparam O type of the data for the suming.
    //! \param pm is a reference to a member function (or variable) of TI. This method must be an accessor and must return a 'O' type.
    template <typename O>
    inline O sum(O (TINP::* pm)() const) const
    {
        O result = 0;
        foreach(TI item,this->_t)
        {
            std::function<O()> memberFunction =  std::bind(pm,item);
            result += memberFunction();
            result += QLinq2R( std::bind(_childreen,item)(), _childreen ).sum(pm);
        }
        return result;
    }

    //! \brief sums a lambda fonction for each item present in the current list recursively
    /// \param func Lambda function to process on one item
    //! \tparam R type of the return type of func
    template <typename R>
    inline R sum(const std::function<R(TI)> &func) const
    {
        R result = 0;
        foreach(TI item,this->_t)
        {
            result += func(item);
            result += QLinq2R( std::bind(_childreen,item)(), _childreen ).sum(func);
        }
        return result;
    }


    //! \brief returns the max value of a property of TI
    //! \tparam O type of the data for the comparaison.
    //! \param pm is a reference to a member function (or variable) of TI. This method must be an accessor and must return a 'O' type.
    template <typename O>
    inline O max(O (TINP::* pm)() const) const
    {
        O result = 0;
        foreach(TI item,this->_t)
        {
            std::function<O()> memberFunction =  std::bind(pm,item);
            result = memberFunction();
            if( std::bind(_childreen,item)().size() != 0)
                result= std::max(result, QLinq2R( std::bind(_childreen,item)(), _childreen ).min(pm));
        }
        return result;
    }

    //! \brief get the maximum value of a lambda fonction for each item present in the current list recursively
    /// \param func Lambda function to process on one item
    //! \tparam R type of the return type of func
    template <typename R>
    inline R max(const std::function<R(TI)> &func) const
    {
        R result = 0;
        foreach(TI item,this->_t)
        {
            result = func(item);
            if( std::bind(_childreen,item)().size() != 0)
                result = std::max(result, QLinq2R( std::bind(_childreen,item)(), _childreen ).min(func));
        }
        return result;
    }

    //! \brief returns the min value of a property of TI
    //! \tparam O type of the data for the comparaison.
    //! \param pm is a reference to a member function (or variable) of TI. This method must be an accessor and must return a 'O' type.
    template <typename O>
    inline O min(O (TINP::* pm)() const) const
    {
        O result = 0;
        foreach(TI item,this->_t)
        {
            std::function<O()> memberFunction =  std::bind(pm,item);
            result = memberFunction();
            if( std::bind(_childreen,item)().size() != 0)
                result= std::min(result, QLinq2R( std::bind(_childreen,item)(), _childreen ).min(pm));
        }
        return result;
    }

    //! \brief get the minimum value of a lambda fonction for each item present in the current list recursively
    /// \param func Lambda function to process on one item
    //! \tparam R type of the return type of func
    template <typename R>
    inline R min(const std::function<R(TI)> &func) const
    {
        R result = 0;
        foreach(TI item,this->_t)
        {
            result = func(item);
            if( std::bind(_childreen,item)().size() != 0)
                result = std::min(result, QLinq2R( std::bind(_childreen,item)(), _childreen ).min(func));
        }
        return result;
    }

     //! \brief Loops on all the items
    inline void forEach(const std::function<void(TI)> &func) const
    {
        foreach(TI item,this->_t)
        {
            func(item);
            if( std::bind(_childreen,item)().size() != 0)
                QLinq2R( std::bind(_childreen,item)(), _childreen ).forEach(func);
        }
    }

    //! \brief Loops on all the items (BFS, Breadth First Search)
    inline void forEachBFS(const std::function<void(TI)> &func) const
    {
       QQueue<TI> myFIFO;
       myFIFO.append(this->_t);

       while(myFIFO.count()!=0)
       {
           TI item = myFIFO.dequeue();
           func(item);
           foreach(TI childItem,std::bind(_childreen,item)())
           {
               myFIFO.enqueue(childItem);
           }
       }
    }

    //! \brief Loops on all the items (BFSR, Breadth First Search Reversed)
    inline void forEachBFSR(const std::function<void(TI)> &func) const
    {
        QStack<TI> myLIFO;

        for(auto i=this->_t.begin(); i < this->_t.end();i++)
        {
            myLIFO.push(*i);
        }

        for(int i=0; i < myLIFO.length(); i++)
        {
            //func(item);
            TI item = myLIFO.at( i );
            foreach(TI childItem,std::bind(_childreen, item)())
            {
                myLIFO.push(childItem);
            }
        }

        while(myLIFO.count()!=0)
        {
            TI item = myLIFO.pop();
            func(item);
        }
    }


  //! \brief ...
  /// \param func Lambda function to process on one item
  //! \tparam R type of the return type of func
  inline QLinq2Q<QList<TI>> where(const std::function<bool(TI)> &func) const
  {
      QList<TI> myItems;
      QQueue<TI> myFIFO;

      myFIFO.append(this->_t);

      while(myFIFO.count()!=0)
      {
          TI item = myFIFO.dequeue();
          if(func(item))
          {
              myItems.append(item);
          }

          foreach(TI childItem,std::bind(_childreen,item)())
          {
              myFIFO.enqueue(childItem);
          }
      }
      return from2q(myItems);
  }

  //! \brief ...
  /// \param func Lambda function to process on one item
  //! \tparam R type of the return type of func
  inline QLinq2Q<QList<TI>> getLevel(long level) const
  {
      QList<TI> myItems;

        if(level==0)
        {
            foreach(auto item, this->_t)
            {
                myItems.append(item);
            }
        }
      else if(level > 0)
      {
          foreach(auto item, this->_t)
          {
              myItems.append( from2R(std::bind(_childreen,item)(), this->_childreen).getLevel(level -1).toQList() );
          }
      }
      return from2q(myItems);
  }
protected:
    const T _t;
    T(TINP::* _childreen)();

    template <typename AVGT> struct AvgTemporaty { AVGT sum; long count; };

private :
    template <typename O>
    inline void _avg(const T &myT, const std::function<O(TI)> &func, AvgTemporaty<O> &avgTemporary) const
    {
        foreach(TI item, myT)
        {
            avgTemporary.count++;
            avgTemporary.sum += func(item);
            if( std::bind(_childreen,item)().size() != 0)
                _avg( std::bind( _childreen, item)(), func, avgTemporary );
        }

    }
}
;

template <class T> QLinq2R<T> from2R(const T & myList, T(std::remove_pointer<typename T::value_type>::type::* const pm)()  )
{
    QLinq2R<T> myQLinq(myList, pm);
    return myQLinq;
}
#endif // QLINQ2R_H
