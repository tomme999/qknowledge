import QtQuick 2.9
import core.backend.viewmodel 1.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import "views"
import "qmlControls"

Page {
    width: 600
    height: 400

    title: qsTr("Home")

    LabelText {
        labelText: "MonTitre"
        text:"Youpi"
    }

    /*ColumnLayout
    {
        ProjectView {
            width: 300
            height: 50

            projectViewModel : ProjectViewModel {
                id:vm;
                name: "Mon Super Projet";
                description:"description du projet qui peut être tres longue ou tres tres courte";
            }
        }
        ProjectView {
            width: 300
            height: 60
            tileColor: "#55aa55";
            projectViewModel : ProjectViewModel {
                id:vm2;
                name: "Mon Super Projet";
                description:"description du projet qui peut être tres longue ou tres tres courte";
            }
        }
        ProjectView {
            width: 300
            height: 70
            tileColor: "#aaaa55";
            projectViewModel : ProjectViewModel {
                id:vm3;
                name: "Mon Super Projet";
                description:"description du projet qui peut être tres longue ou tres tres courte";
            }
        }
        ProjectView {
            width: 300
            height: 80
            tileColor: "#aa55aa";
            projectViewModel : ProjectViewModel {
                id:vm34;
                name: "Mon Super Projet";
                description:"description du projet qui peut être tres longue ou tres tres courte";
            }
        }
        ProjectView {
            width: 300
            height: 90
            tileColor: "#5555aa";
            projectViewModel : ProjectViewModel {
                id:vm4;
                name: "Mon Super Projet";
                description:"description du projet qui peut être tres longue ou tres tres courte";
            }
        }
    }*/
    Label {
        text: qsTr("You are on the home page.")
        anchors.centerIn: parent
    }
}
