CREATE TABLE `Contact` (
        `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        `name`	BLOB NOT NULL,
        `firstName`	REAL NOT NULL,
        `email`	TEXT NOT NULL,
        `creation`	TEXT NOT NULL,
        `modification`	TEXT NOT NULL,
        `phoneNumber`	TEXT NOT NULL
);

CREATE TABLE "Task" (
        "id"	INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        "name"          TEXT NOT NULL,
        "description"	TEXT NOT NULL,
        "endDateWanted"	TEXT NOT NULL,
        "idRoleOwner"	INTEGER NOT NULL,
        "idProject"	INTEGER NOT NULL,
        `creation`	TEXT NOT NULL,
        `modification`	TEXT NOT NULL
);

CREATE TABLE "Risk" (
        "id"	INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        "startDate"         TEXT NOT NULL,
        "name"              TEXT NOT NULL,
        "description"       TEXT NOT NULL,
        "level"             INTEGER NOT NULL,
        "Probability"       INTEGER NOT NULL,
        "idContactOwner"    INTEGER NOT NULL,
        "actionToLead"      TEXT,
        "isClosed"          INTEGER NOT NULL,
        "endDateWanted"     TEXT NOT NULL,
        "idProject"         INTEGER NOT NULL,
        `creation`          TEXT NOT NULL,
        `modification`      TEXT NOT NULL
);

CREATE TABLE "Project"(
        "id"	INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        "name"              TEXT NOT NULL,
        "description"       TEXT NOT NULL,
        `creation`          TEXT NOT NULL,
        `modification`      TEXT NOT NULL
);

CREATE TABLE "Question"(
        "id"	INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        "question"              TEXT NOT NULL,
        "answer"                TEXT NOT NULL,
        "answerDate"            TEXT NOT NULL,
        "wanted"                TEXT NOT NULL,
        "idproject"             INTEGER NOT NULL,
        `creation`              TEXT NOT NULL,
        `modification`          TEXT NOT NULL
);
