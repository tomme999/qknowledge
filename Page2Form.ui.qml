import QtQuick 2.9
import QtQuick.Controls 2.2
import core.backend.viewmodel 1.0
import QtQuick.Layouts 1.3

Page {
    id: root
    width: 600
    height: 400
    property bool enabled: true
    property ContactViewModel contactViewModel: ContactViewModel {
        name: txtName.text
        firstName: txtFirstName.text
        phoneNumber: txtPhone.text
        email: txtEmail.text
    }
    signal close

    title: qsTr("Page 2")

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent

        Label {
            id: lblName
            text: qsTr("Nom :")
            font.pointSize: 9
            height: 50
        }

        TextField {
            id: txtName
            text: contactViewModel.name
            height: 50
            enabled: root.enabled
        }

        Label {
            id: lblFirstName
            text: qsTr("Prénom :")
            font.pointSize: 9
            height: 50
        }

        TextField {
            id: txtFirstName
            text: contactViewModel.firstName
            height: 50
            enabled: root.enabled
        }

        Label {
            id: lblPhone
            text: qsTr("Téléphone :")
            font.pointSize: 9
            height: 50
        }

        TextField {
            id: txtPhone
            text: contactViewModel.phoneNumber
            height: 50
            enabled: root.enabled
        }

        Label {
            id: lblEmail
            text: qsTr("Email :")
            font.pointSize: 9
            height: 50
        }

        TextField {
            id: txtEmail
            text: contactViewModel.email
            height: 50
            enabled: root.enabled
        }

        Button {
            id: butSave
            text: qsTr("Enregistrer")
        }
    }

    Connections {
        target: butSave
        onClicked: {
            contactViewModel.store()
            root.close()
        }
    }

    BorderImage {
        id: borderImage
        x: 209
        y: 47
        width: 100
        height: 100
        //source: "qrc:/qtquickplugin/images/template_image.png"
        source: "qrc:/assets/BrainLogo.jpg"
    }
}




/*##^## Designer {
    D{i:2;anchors_height:400;anchors_width:200;anchors_x:71;anchors_y:25}
}
 ##^##*/
