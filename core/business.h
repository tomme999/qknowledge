#ifndef BUSINESS_H
#define BUSINESS_H

#include <QDateTime>
#include <QObject>
#include <QSharedPointer>
#include <QString>

#define __SETTER__(inVal) { if (m_##inVal == inVal) return; m_##inVal = inVal; emit inVal##Changed(m_##inVal); }
//#define _VSETTER__(inVal) { if (m_##inVal == inVal && !inVal##ValidationRule(inVal) ) return; m_##inVal = inVal; emit inVal##Changed(m_##inVal); }
#define _VSETTER__(inNameSpace, inVal) { if (m_##inVal == inVal && !inNameSpace::inVal##ValidationRule(inVal) ) return; m_##inVal = inVal; emit inVal##Changed(m_##inVal); }


class Business : public QObject
{
    Q_OBJECT

    Q_PROPERTY(long         id              READ id             WRITE setId             NOTIFY idChanged)           // Id de la question
    Q_PROPERTY(QDateTime    creation        READ creation       WRITE setCreation   )                               // Date de creation
    Q_PROPERTY(QDateTime    modification    READ modification   WRITE setModification   NOTIFY modificationChanged) // Date de modificaiton

    long        m_id;
    QDateTime   m_creation;
    QDateTime   m_modification;
    bool        m_isDeleted;

public:

    explicit Business(QObject *parent = nullptr);

    long        id()                        const   {   return m_id;            }
    QDateTime   creation()                  const   {   return m_creation;      }
    QDateTime   modification()              const   {   return m_modification;  }
    bool        isDeleted()                 const   {   return m_isDeleted;     }

    static QString getIdPropertyName()          {  return "id";             }

signals:

    void idChanged                  (   long id                     );
    void modificationChanged        (   QDateTime modification      );

public slots:

    void setId                      (   long id                     )    __SETTER__(   id              )
    void setCreation                (   QDateTime creation          )    {    m_creation = creation;   }
    void setModification            (   QDateTime modification      )    __SETTER__(   modification    )

};

#endif // BUSINESS_H
