﻿#ifndef DBSERVICEHELPER_H
#define DBSERVICEHELPER_H

#include <QObject>
#include <QList>
#include <QMetaObject>
#include <QMetaProperty>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QString>
#include <QVariant>
#include "business.h"

class DBServiceHelper
{
public:
    inline static QString  bindedVarName        (   QString fieldName ) { return ":"+fieldName.toLower();   }

    static QList<QString>  loadProperties       (   const   QMetaObject &metaObject                                                                         );
    static QString         generateFieldList    (           QList<QString> &fieldList,  const   QString &idFieldsName                                       );
    static QString         generateSelect       (   const   QMetaObject &metaObject,            QString &fields                                             );
    static QString         generateUpdate       (   const   QMetaObject &metaObject,            QList<QString> &fieldList,      const QString &idFieldsName );
    static QString         generateDelete       (   const   QMetaObject &metaObject,    const   QString &idFieldsName                                       );
    static QString         generateInsert       (   const   QMetaObject &metaObject,            QList<QString> &fieldList,      const QString &idFieldsName );

    static void            populate             (           QSqlQuery &qSqlQuery,       const   QMetaProperty &qMetaProperty,    QObject &qObject       )        {  qSqlQuery.bindValue(bindedVarName(qMetaProperty.name()), qMetaProperty.read(&qObject)); }
    static bool            populate             (           QObject &qObject,           const   QMetaProperty &qMetaProperty,    QSqlQuery &qSqlQuery   )        {  return qMetaProperty.write(&qObject,qSqlQuery.value(qMetaProperty.name()));             }
    static void            populate             (           QSqlQuery &qSqlQuery,               QObject &qObject                                            );
    static bool            populate             (           QObject &qObject,                   QSqlQuery &qSqlQuery                                        );

    static bool            insert               (   const   QString &Query,                     QObject &qObject       , const QMetaProperty &qMetaProperty );
    static bool            update               (   const   QString &Query,                     QObject &qObject                                            );
    static bool            remove               (   const   QString &Query,                     Business &qObject                                           );
    static bool            select               (   const   QString &Query,                     QObject &qObject                                            );
    static bool            select               (   const   QString &Query,             const   QString &idFieldsName,  long id, Business &qObject          );
    //static bool            selectAll            (   const   QString &Query,             const   QString &idFieldsName,  long id, QObject &qObject     );
    static bool            store                (   const   QString &QueryInsert,       const   QString &QueryUpdate,           Business &qObject           );

    static bool            prepareQuery         (   QSqlQuery &qSqlQuery    ,           const   QString &strQuery   );
    static bool            execQuery            (   QSqlQuery &qSqlQuery                                            );
};


#endif // DBSERVICEHELPER_H
