#ifndef BUSINESSRULES_H
#define BUSINESSRULES_H

#define BUSINESS_VALIDATION_RULE(TYPE,MEMBER,CODE) inline bool MEMBER##ValidationRule(TYPE MEMBER) { CODE ;}

#endif // BUSINESSRULES_H
