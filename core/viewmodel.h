#ifndef VIEWMODEL_H
#define VIEWMODEL_H
#include <QObject>

namespace core
{
    class ViewModel : public QObject
    {
        Q_OBJECT
    public:
        explicit ViewModel   (QObject *parent = nullptr) : QObject(parent) {}

        Q_INVOKABLE virtual bool load   (long id=-1)    = 0;
        Q_INVOKABLE virtual bool store  ()              = 0;
        Q_INVOKABLE virtual bool cancel ()              = 0;
        Q_INVOKABLE virtual bool refresh ()             = 0;

    };
}
#endif // VIEWMODEL_H
