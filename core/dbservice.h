#ifndef DBSERVICE_H
#define DBSERVICE_H

#include <QDebug>
#include <QObject>
#include <QList>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QSharedPointer>
#include <QString>
#include <QMetaProperty>
#include <QMap>

#include "dbservicehelper.h"

template <class BUSINESS> class DBService
{
public:
    typedef QSharedPointer<BUSINESS> ShrPtr;
    typedef QList<ShrPtr>            LstShrPtr;
    typedef QMap<int,ShrPtr>         MapShrPtr;

    DBService() :
        m_properties        (   p_loadProperties()      ),
        m_fieldsList        (   p_generate_fieldList()  ),
        m_select            (   p_generate_select()     ),

        m_delete            (   p_generate_delete()     ),
        m_insert            (   p_generate_insert()     ),
        m_update            (   p_generate_update()     )
    {}

    virtual ShrPtr          getNew  ()                         const   {    return ShrPtr(new BUSINESS());                                                  }
    virtual bool            store   (   ShrPtr  business    )  const   {    return DBServiceHelper::store(m_insert, m_update, *business);                   }
    virtual bool            remove  (   ShrPtr  business    )  const   {    return DBServiceHelper::remove(m_delete,*business);                             }
    virtual ShrPtr          get     (   long    id          )  const   {    ShrPtr sp(new BUSINESS()); if(DBServiceHelper::select(m_select, BUSINESS::getIdPropertyName(), id, *sp)) return sp; sp.clear(); return sp;    }
    virtual LstShrPtr       get     ()                         const   {    auto r=this->selectAll(); return r;   }

protected:

    QList<QString>  m_properties;       // content BUSINESS properties
    QString         m_fieldsList;
    QString         m_select;
    QString         m_delete;
    QString         m_insert;
    QString         m_update;

    //virtual bool    populate    (   QSqlQuery   &qSqlQuery, const BUSINESS    &business    )    {   return true;   }

private:

    QList<QString>  p_loadProperties()         {    return DBServiceHelper::loadProperties     (   BUSINESS::staticMetaObject                                               );     }
    QString         p_generate_fieldList()     {    return DBServiceHelper::generateFieldList  (   m_properties,               BUSINESS::getIdPropertyName()                );     }
    QString         p_generate_select()        {    return DBServiceHelper::generateSelect     (   BUSINESS::staticMetaObject, m_fieldsList                                 );     }
    QString         p_generate_delete()        {    return DBServiceHelper::generateDelete     (   BUSINESS::staticMetaObject, BUSINESS::getIdPropertyName()                );     }
    QString         p_generate_insert()        {    return DBServiceHelper::generateInsert     (   BUSINESS::staticMetaObject, m_properties, BUSINESS::getIdPropertyName()  );     }
    QString         p_generate_update()        {    return DBServiceHelper::generateUpdate     (   BUSINESS::staticMetaObject, m_properties, BUSINESS::getIdPropertyName()  );     }

    LstShrPtr       selectAll () const
    {
        QSqlQuery q(m_select);
        LstShrPtr lst;
        //if(DBServiceHelper::prepareQuery(q,m_select))
        if(true)
        {
            qDebug() << q.lastError().text();
            qDebug() << q.lastQuery();
            while(q.next())
            {
                ShrPtr p(new BUSINESS ());
                DBServiceHelper::populate( *p, q);
                lst.append(p);
            }
        }

        return lst;
    }
};

#endif // DBSERVICE_H
