#ifndef QDATEVALIDATOR_H
#define QDATEVALIDATOR_H

#include <QObject>
#include <QDate>
#include <QValidator>

class QDateValidator : public QValidator
{
    Q_OBJECT
public:
    explicit QDateValidator(QObject *parent = nullptr);

signals:

public slots:

    // QValidator interface
public:
    virtual State validate(QString &, int &) const;
//    virtual void fixup(QString &) const;
};

#endif // QDATEVALIDATOR_H
