#include "qdatetimevalidator.h"

QDateTimeValidator::QDateTimeValidator(QObject *parent) : QValidator(parent)
{

}

QValidator::State QDateTimeValidator::validate(QString &s, int &p) const
{
    Q_UNUSED(p)

    QDateTime value = QDateTime::fromString(s,"d/M/yyyy h:m");
    if(value.isValid())
    {
        return QValidator::Acceptable;
    }
    if(s.length()<16)
    {
        return QValidator::Intermediate;
    }

    return QValidator::Invalid;
}
