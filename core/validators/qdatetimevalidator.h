#ifndef QDATETIMEVALIDATOR_H
#define QDATETIMEVALIDATOR_H

#include <QObject>
#include <QDateTime>
#include <QValidator>

class QDateTimeValidator : public QValidator
{
    Q_OBJECT
public:
    explicit QDateTimeValidator(QObject *parent = nullptr);

signals:

public slots:

public:
    virtual State validate(QString &, int &) const;
};

#endif // QDATETIMEVALIDATOR_H
