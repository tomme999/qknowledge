#include "qdatevalidator.h"

QDateValidator::QDateValidator(QObject *parent) : QValidator(parent)
{

}

QValidator::State QDateValidator::validate(QString &s, int &p) const
{
    Q_UNUSED(p)

    QDate value = QDate::fromString(s,"d/M/yyyy");
    if(value.isValid())
    {
        return QValidator::Acceptable;
    }
    if(s.length()<10)
    {
        return QValidator::Intermediate;
    }

    return QValidator::Invalid;
}
