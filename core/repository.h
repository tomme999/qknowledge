#ifndef REPOSITORY_H
#define REPOSITORY_H

namespace core {
    class Repository
    {
    public:
        template <class A> A * get()    {   return new A(); }
    };
}

#endif // REPOSITORY_H
