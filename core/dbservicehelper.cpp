#include "dbservicehelper.h"
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QDebug>

QList<QString> DBServiceHelper::loadProperties(const QMetaObject &metaObject)
{
    QList<QString>  m_properties;
    for(int i =1;   i<metaObject.propertyCount();   i++)
    {
       QMetaProperty qMetaProperty = metaObject.property(i);
       m_properties << qMetaProperty.name();
    }
    return m_properties;
}

QString DBServiceHelper::generateFieldList(QList<QString> &fieldList, const QString &idFieldName)
{
    Q_UNUSED(idFieldName);

    QString a;
    foreach(QString b,fieldList)
    {
        a += (a=="")?b:","+b;
    }
    return a;
}

QString DBServiceHelper::generateSelect(const QMetaObject &metaObject, QString &fields)
{
    return QString("SELECT %1 FROM %2").arg(fields).arg(metaObject.className());
}

QString DBServiceHelper::generateUpdate(const QMetaObject &metaObject, QList<QString> &fieldList, const QString &idFieldsName)
{
    QString request;
    foreach(QString b, fieldList)
    {
        if(b!=idFieldsName)
        {
            request += QString("%3 %1 = %2")
                    .arg(b)
                    .arg(bindedVarName(b))
                    .arg((request=="")?"":", ");
        }
    }
    return QString("UPDATE %1 SET %2 where %3 = %4")
            .arg(metaObject.className())
            .arg(request)
            .arg(idFieldsName)
            .arg(bindedVarName(idFieldsName));
}

QString DBServiceHelper::generateDelete(const QMetaObject &metaObject, const QString &idFieldsName)
{
    return QString("DELETE FROM %1 WHERE %2=%3")
            .arg(metaObject.className())
            .arg(idFieldsName)
            .arg(bindedVarName(idFieldsName));
}

QString DBServiceHelper::generateInsert(const QMetaObject &metaObject, QList<QString> &fieldList, const QString &idFieldsName)
{
    QString fields, vars;
    foreach(QString b, fieldList)
    {
        if(b!=idFieldsName)
        {
            fields += (fields=="")?b:", "+b;
            vars += ((vars=="")?"":",") + bindedVarName(b);
        }
    }
    return QString("INSERT INTO %1 (%2) VALUES (%3)")
            .arg(metaObject.className())
            .arg(fields)
            .arg(vars);
}

void DBServiceHelper::populate(QSqlQuery &qSqlQuery, QObject &qObject)
{
    auto meta = qObject.metaObject();
    for(    int i=1;    i<meta->propertyCount();    i++)
    {
        auto metaProperty = meta->property(i);
        populate(   qSqlQuery,  metaProperty,   qObject);
    }
}

bool DBServiceHelper::populate(QObject &qObject, QSqlQuery &qSqlQuery)
{
    QSqlRecord qSqlRecord = qSqlQuery.record();

    auto meta = qObject.metaObject();

    for(int i=1;    i<meta->propertyCount();   i++)
    {
        QVariant a = qSqlQuery.record().value(meta->property(i).name());
        if( a.isValid())
            DBServiceHelper::populate(qObject,   meta->property(i),   qSqlQuery);
    }
    return true;
}

bool DBServiceHelper::insert(const QString &Query, QObject &qObject,  QMetaProperty const & qMetaProperty )
{

    QSqlQuery qSqlQuery;

    if( !DBServiceHelper::prepareQuery(qSqlQuery, Query) )
        return false;

    populate(   qSqlQuery,  qObject );

    if( DBServiceHelper::execQuery(qSqlQuery) )
        return qMetaProperty.write( &qObject,   qSqlQuery.lastInsertId());
    else
        return false;
}

bool DBServiceHelper::update(const QString &Query, QObject &qObject)
{
    QSqlQuery qSqlQuery;

    if( !DBServiceHelper::prepareQuery(qSqlQuery, Query) )
        return false;

    populate(   qSqlQuery,  qObject );

    return DBServiceHelper::execQuery(qSqlQuery);
}

bool DBServiceHelper::remove(const QString &Query, Business &qObject)
{
    QSqlQuery qSqlQuery;

    if( !DBServiceHelper::prepareQuery(qSqlQuery, Query) )
        return false;

    populate(qSqlQuery, qObject);
    return DBServiceHelper::execQuery(qSqlQuery);
}

bool DBServiceHelper::select(const QString &Query, QObject &qObject)
{
    QSqlQuery qSqlQuery(Query);

    while (qSqlQuery.next())
    {
        populate(qObject,   qSqlQuery);
    }
    return true;
}

bool DBServiceHelper::select(const QString &Query, const QString &idFieldsName, long id, Business &qObject)
{
    bool returnValue(false);

    QString lQuery = QString("%1 WHERE %2 = %3").arg(Query).arg(idFieldsName).arg(bindedVarName(idFieldsName));

    QSqlQuery qSqlQuery;
    auto lId = QVariant::fromValue(id);
    auto strId = bindedVarName(idFieldsName);

    if(!prepareQuery(qSqlQuery, lQuery))
        return false;

    qSqlQuery.bindValue(strId,lId);

    if(!qSqlQuery.exec())
        return false;

    while (qSqlQuery.next())
    {
        populate(qObject,   qSqlQuery);
        returnValue = true;
    }

    return returnValue;
}

//bool DBServiceHelper::selectAll(const QString &Query, const QString &idFieldsName, long id, QObject &qObject)
//{
//    bool returnValue(false);

//    QString lQuery = QString("%1 WHERE %2 = %3").arg(Query).arg(idFieldsName).arg(bindedVarName(idFieldsName));

//    QSqlQuery qSqlQuery;
//    auto lId = QVariant::fromValue(id);
//    auto strId = bindedVarName(idFieldsName);

//    if(!prepareQuery(qSqlQuery, lQuery))
//        return false;

//    qSqlQuery.bindValue(strId,lId);

//    if(!qSqlQuery.exec())
//        return false;

//    while (qSqlQuery.next())
//    {
//        populate(qObject,   qSqlQuery);
//        returnValue = true;
//    }

//    return returnValue;
//}



bool DBServiceHelper::store(const QString &QueryInsert, const QString &QueryUpdate, Business &qObject)
{
    auto index              = qObject.metaObject()->indexOfProperty(qObject.getIdPropertyName().toStdString().c_str());
    QMetaProperty property  = qObject.metaObject()->property(index);
    QVariant id             = property.read(&qObject);

    qObject.setModification(QDateTime::currentDateTime());

    if(id.toInt() == -1)
    {
        return insert(QueryInsert, qObject,   property);
    }
    else
    {
        return update(QueryUpdate, qObject)    ;
    }
}

bool DBServiceHelper::prepareQuery(QSqlQuery &qSqlQuery, const QString &strQuery)
{
    if(!qSqlQuery.prepare(strQuery))
    {
        qCritical() << "Error: prepare "<<strQuery<<qSqlQuery.lastError();
        return false;
    }
    return true;
}

bool DBServiceHelper::execQuery(QSqlQuery &qSqlQuery)
{
    if(qSqlQuery.exec())
        return true;
    else
    {
        qCritical()<< qSqlQuery.lastError();
        return false;
    }
}
