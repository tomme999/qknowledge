#include "questionlistviewmodel.h"
#include <repositories/viewmodelrepository.h>

QuestionListViewModel::QuestionListViewModel(QObject *parent) : ViewModel(parent),
    m_Model(ModelRepository::instance().get<QuestionListModel>(this))
{

}

QuestionListViewModel::QuestionListViewModel(QuestionListModel *model,QObject *parent)
    : ViewModel(parent)
    , m_Model(model)
{

}

bool QuestionListViewModel::load(long id)
{
    if(id!=-1 && m_Model->load(id))
        return this->populateViewModel();
    if( m_Model->load())
        return this->populateViewModel();
    return false;
}

bool QuestionListViewModel::refresh()
{
    return this->populateViewModel();
}

bool QuestionListViewModel::store()
{
    qFatal("methode store() in QuestionListViewModel not implemented");
}

bool QuestionListViewModel::cancel()
{
    qFatal("methode cancel() in QuestionListViewModel not implemented");
}

bool QuestionListViewModel::edit()
{
    if(m_questionListViewModel.count()!=0)
    {
        ViewModelRepository::instance().bridge()->openQuestionEditAsk(static_cast<QuestionViewModel *>(m_questionListViewModel[0]));
        return true;
    }
    else
    {
        return false;
    }
}

bool QuestionListViewModel::populateViewModel()
{
    m_questionListViewModel.clear();
    foreach(QuestionService::ShrPtr u, this->m_Model->questionsList())
    {
        //auto a = new ProjectViewModel(u.data(),this);
        QuestionModel * questionModel = new QuestionModel(u, this);
        QuestionViewModel * questionViewModel = new QuestionViewModel(questionModel, this);

        m_questionListViewModel <<  questionViewModel;
    }
    emit questionListViewModelChanged( m_questionListViewModel );

    return true;
}

bool QuestionListViewModel::populateModel()
{
    qFatal("methode populateModel() in QuestionListViewModel not implemented");
}
