#include "todoviewmodel.h"

TodoViewModel::TodoViewModel(QObject *parent) : ViewModel(parent),
    m_Model( ModelRepository::instance().get<TodoModel>(this) )
{

}

TodoViewModel::TodoViewModel(TodoModel *model, QObject *parent) :
    core::ViewModel (parent),
    m_Model( model )
{
    this->populateViewModel();
}

bool TodoViewModel::load(long id)
{
    if(m_Model->load(id))
        return this->populateViewModel();
    return false;
}

bool TodoViewModel::store()
{
    if(this->populateModel())
        return m_Model->store();
    return false;
}

bool TodoViewModel::cancel()
{
    return true;
}

bool TodoViewModel::refresh()
{
    return this->populateViewModel();
}


bool TodoViewModel::populateViewModel()
{
    if(m_Model->todo().isNull())
        return false;

    auto model = m_Model->todo();

    this->setId             ( model->id()           );
    this->setName           ( model->name()         );
    this->setDescription    ( model->description()  );
    this->setEndDateWanted  ( model->endDateWanted());
    this->setIdRoleOwner    ( model->idRoleOwner()  );
    this->setDone           ( model->done()         );

    return true;
}

bool TodoViewModel::populateModel()
{
    // desynchronisation between model and viewmodel
    if(m_Model->todo()->id()!=this->id())
        return false;

    auto model = m_Model->todo();

    model->setId             ( this->id()           );
    model->setName           ( this->name()         );
    model->setDescription    ( this->description()  );
    model->setEndDateWanted  ( this->endDateWanted());
    model->setIdRoleOwner    ( this->idRoleOwner()  );
    model->setDone           ( this->done()         );

    return true;
}
