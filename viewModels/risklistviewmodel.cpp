#include "risklistviewmodel.h"
#include <repositories/viewmodelrepository.h>

RiskListViewModel::RiskListViewModel(QObject *parent) : ViewModel(parent),
    m_Model(ModelRepository::instance().get<RiskListModel>(this))
{

}
RiskListViewModel:: RiskListViewModel(RiskListModel *model, QObject *parent)
    : ViewModel(parent),
    m_Model(model)
{

}

bool RiskListViewModel::load(long id)
{
    if(id!=-1 && m_Model->load(id))
        return this->populateViewModel();
    if( m_Model->load())
        return this->populateViewModel();
    return false;
}

bool RiskListViewModel::refresh()
{
    return this->populateViewModel();
}


bool RiskListViewModel::store()
{
    qFatal("methode store() in QuestionListViewModel not implemented");
}

bool RiskListViewModel::cancel()
{
    qFatal("methode cancel() in QuestionListViewModel not implemented");
}

bool RiskListViewModel::edit()
{
    if(m_riskListViewModel.count()!=0)
    {
        ViewModelRepository::instance().bridge()->openRiskEditAsk(static_cast<RiskViewModel *>(m_riskListViewModel[0]));
        return true;
    }
    else
    {
        return false;
    }
}

bool RiskListViewModel::populateViewModel()
{
    m_riskListViewModel.clear();
    foreach(RiskService::ShrPtr u, this->m_Model->risksList())
    {
        //auto a = new ProjectViewModel(u.data(),this);
        RiskModel * riskModel = new RiskModel(u, this);
        RiskViewModel * questionViewModel = new RiskViewModel(riskModel, this);

        m_riskListViewModel <<  questionViewModel;
    }
    emit riskListViewModelChanged( m_riskListViewModel );

    return true;
}

bool RiskListViewModel::populateModel()
{
    qFatal("methode populateModel() in QuestionListViewModel not implemented");
}
