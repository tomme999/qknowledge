#ifndef RISKVIEWMODEL_H
#define RISKVIEWMODEL_H

#include <QObject>
#include <core/viewmodel.h>
#include <repositories/modelrepository.h>
#include <repositories/constantrepository.h>
#include <models/riskmodel.h>
#include <business/rules/riskrules.h>
#include <business/risk.h>

class RiskViewModel : public core::ViewModel
{
    Q_OBJECT
    Q_PROPERTY(QDate        startDate       READ    startDate       WRITE setStartTime      NOTIFY startDateChanged         )
    Q_PROPERTY(QString      name            READ    name            WRITE setName           NOTIFY nameChanged              )
    Q_PROPERTY(QString      description     READ    description     WRITE setDescription    NOTIFY descriptionChanged       )
    Q_PROPERTY(int          level           READ    level           WRITE setLevel          NOTIFY levelChanged             )
    Q_PROPERTY(int          probability     READ    probability     WRITE setProbability    NOTIFY probabilityChanged       )
    Q_PROPERTY(long         idContactOwner  READ    idContactOwner  WRITE setIdContactOwner NOTIFY idContactOwnerChanged    )
    Q_PROPERTY(QString      actionToLead    READ    actionToLead    WRITE setActionToLead   NOTIFY actionToLeadChanged      )
    Q_PROPERTY(bool         isClosed        READ    isClosed        WRITE setIsClosed       NOTIFY isClosedChanged          )
    Q_PROPERTY(QDate        endDateWanted   READ    endDateWanted   WRITE setEndDateWanted  NOTIFY endDateWantedChanged     )
    Q_PROPERTY(QColor       colorRisk       READ    colorRisk       CONSTANT)

    QDate           m_startDate       ;
    QString         m_name            ;
    QString         m_description     ;
    int             m_level           ;
    int             m_probability     ;
    long            m_idContactOwner  ;
    QString         m_actionToLead    ;
    bool            m_isClosed        ;
    QDate           m_endDateWanted   ;
public:
    explicit RiskViewModel(QObject *parent = nullptr);
    explicit RiskViewModel(RiskModel* model, QObject *parent = nullptr);

    Q_INVOKABLE bool load   (long id=-1)    ;
    Q_INVOKABLE bool store  ()              ;
    Q_INVOKABLE bool cancel ()              ;
    Q_INVOKABLE bool refresh()              ;

    QDate           startDate      () const {    return m_startDate      ; }
    QString         name           () const {    return m_name           ; }
    QString         description    () const {    return m_description    ; }
    int             level          () const {    return m_level          ; }
    int             probability    () const {    return m_probability    ; }
    long            idContactOwner () const {    return m_idContactOwner ; }
    QString         actionToLead   () const {    return m_actionToLead   ; }
    bool            isClosed       () const {    return m_isClosed       ; }
    QDate           endDateWanted  () const {    return m_endDateWanted  ; }
    QColor          colorRisk      () const {    return ConstantRepository::instance().getRiskColor(m_level,m_probability); }

signals:

    void    startDateChanged            (     QDate         startDate         );
    void    nameChanged                 (     QString       name              );
    void    descriptionChanged          (     QString       description       );
    void    levelChanged                (     int           level             );
    void    probabilityChanged          (     int           probability       );
    void    idContactOwnerChanged       (     long          idContactOwner    );
    void    actionToLeadChanged         (     QString       actionToLead      );
    void    isClosedChanged             (     bool          isClosed          );
    void    endDateWantedChanged        (     QDate         endDateWanted     );

public slots:

    void    setStartTime                (     QDate         startDate         )   _VSETTER__( RiskRules,       startDate         )
    void    setName                     (     QString       name              )   _VSETTER__( RiskRules,       name              )
    void    setDescription              (     QString       description       )   _VSETTER__( RiskRules,       description       )
    void    setLevel                    (     int           level             )   _VSETTER__( RiskRules,       level             )
    void    setProbability              (     int           probability       )   _VSETTER__( RiskRules,       probability       )
    void    setIdContactOwner           (     long          idContactOwner    )   _VSETTER__( RiskRules,       idContactOwner    )
    void    setActionToLead             (     QString       actionToLead      )   _VSETTER__( RiskRules,       actionToLead      )
    void    setIsClosed                 (     bool          isClosed          )   __SETTER__( isClosed          )
    void    setEndDateWanted            (     QDate         endDateWanted     )   __SETTER__( endDateWanted     )

protected:
    bool populateViewModel();
    bool populateModel();

private:
    RiskModel    *m_Model;
};

#endif // RISKVIEWMODEL_H
