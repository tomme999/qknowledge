#ifndef TODOVIEWMODEL_H
#define TODOVIEWMODEL_H

#include <QObject>
#include <QColor>
#include <core/viewmodel.h>
#include <repositories/modelrepository.h>
#include <models/todomodel.h>
#include <business/rules/todorules.h>
#include <repositories/constantrepository.h>

class TodoViewModel : public core::ViewModel
{
    Q_OBJECT

    Q_PROPERTY(long         id              READ    id              WRITE setId             NOTIFY idChanged                )
    Q_PROPERTY(QDate        endDateWanted   READ    endDateWanted   WRITE setEndDateWanted  NOTIFY endDateWantedChanged     )
    Q_PROPERTY(QString      description     READ    description     WRITE setDescription    NOTIFY descriptionChanged       )
    Q_PROPERTY(QString      name            READ    name            WRITE setName           NOTIFY nameChanged              )
    Q_PROPERTY(long         idRoleOwner     READ    idRoleOwner     WRITE setIdRoleOwner    NOTIFY idRoleOwnerChanged       )
    Q_PROPERTY(bool         done            READ    done            WRITE setDone           NOTIFY doneChanged              )

    long                    m_id            ;
    QString                 m_name          ;
    QString                 m_description   ;
    QDate                   m_endDateWanted ;
    long                    m_idRoleOwner   ;
    bool                    m_done          ;

public:
    explicit TodoViewModel(QObject *parent = nullptr);
    explicit TodoViewModel(TodoModel* model, QObject *parent = nullptr);

    long    id            () const      {   return  m_id            ;       }
    QDate   endDateWanted () const      {   return  m_endDateWanted ;       }
    QString description   () const      {   return  m_description   ;       }
    QString name          () const      {   return  m_name          ;       }
    long    idRoleOwner   () const      {   return  m_idRoleOwner   ;       }
    bool    done          () const      {   return  m_done          ;       }

    Q_INVOKABLE bool load   (long id=-1)    ;
    Q_INVOKABLE bool store  ()              ;
    Q_INVOKABLE bool cancel ()              ;
    Q_INVOKABLE bool refresh()              ;

signals:

    void    idChanged                   (    long        id                 );
    void    descriptionChanged          (    QString     description        );
    void    endDateWantedChanged        (    QDate       endDateWanted      );
    void    idRoleOwnerChanged          (    long        idRoleOwner        );
    void    nameChanged                 (    QString     name               );
    void    doneChanged                 (    bool        done               );

public slots:

    void    setId                       (    long        id                 )    _VSETTER__( TodoRules,  id                 )
    void    setDescription              (    QString     description        )    _VSETTER__( TodoRules,  description        )
    void    setEndDateWanted            (    QDate       endDateWanted      )    _VSETTER__( TodoRules,  endDateWanted      )
    void    setIdRoleOwner              (    long        idRoleOwner        )    _VSETTER__( TodoRules,  idRoleOwner        )
    void    setName                     (    QString     name               )    _VSETTER__( TodoRules,  name               )
    void    setDone                     (    bool        done               )    _VSETTER__( TodoRules,  done               )

    protected:
        bool populateViewModel();
        bool populateModel();

    private:
        TodoModel    *m_Model;

};

#endif // TODOVIEWMODEL_H
