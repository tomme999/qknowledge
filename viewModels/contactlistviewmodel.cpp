#include "contactlistviewmodel.h"

ContactListViewModel::ContactListViewModel(QObject *parent)
    : ViewModel(parent)
    , m_Model(ModelRepository::instance().get<ContactListModel>(this))
{
}

bool ContactListViewModel::load(long id)
{
    if(id!=-1 && m_Model->load(id))
        return this->populateViewModel();
    if( m_Model->load())
        return this->populateViewModel();
    return false;
}

bool ContactListViewModel::store()
{
    qFatal("methode store() in ContactListViewModel not implemented");
}

bool ContactListViewModel::cancel()
{
    qFatal("methode cancel() in ContactListViewModel not implemented");
}

bool ContactListViewModel::refresh()
{
    return this->populateViewModel();
}

bool ContactListViewModel::populateViewModel()
{
    m_contactListViewModel.clear();
    foreach(ContactModel *contactModel, this->m_Model->contactModels())
    {
        ContactViewModel * contactViewModel = new ContactViewModel(contactModel, this);
        this->m_contactListViewModel << contactViewModel;
    }
    emit contactListViewModelChanged( m_contactListViewModel );
    return true;
}

bool ContactListViewModel::populateModel()
{
    qFatal("methode populateModel() in ContactListViewModel not implemented");
}
