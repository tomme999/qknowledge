#ifndef VIEWMODELSBRIDGE_H
#define VIEWMODELSBRIDGE_H

#include <QObject>
#include <viewModels/contactviewmodel.h>
#include <viewModels/projectviewmodel.h>
#include <viewModels/questionviewmodel.h>
#include <viewModels/taskviewmodel.h>
#include <viewModels/riskviewmodel.h>

class ViewModelsBridge : public QObject
{
    Q_OBJECT
public:
    explicit ViewModelsBridge(QObject *parent = nullptr);

signals:

    /* signal to create a new view */
    void openContactDetailsAsked        (   ContactViewModel    *vm     );
    void openQuestionDetailsAsked       (   QuestionViewModel   *vm     );
    void openTaskDetailsAsked           (   TaskViewModel       *vm     );
    void openRiskDetailsAsked           (   RiskViewModel       *vm     );

    void openContactEditAsked           (   ContactViewModel    *vm     );
    void openQuestionEditAsked          (   QuestionViewModel   *vm     );
    void openTaskEditAsked              (   TaskViewModel       *vm     );
    void openRiskEditAsked              (   RiskViewModel       *vm     );

public slots:

    /* slot to ask a new view */
    void openContactDetailsAsk     (   ContactViewModel    *vm    )   {   emit openContactDetailsAsked    ( vm );   }
    void openQuestionDetailsAsk    (   QuestionViewModel   *vm    )   {   emit openQuestionDetailsAsked   ( vm );   }
    void openTaskDetailsAsk        (   TaskViewModel       *vm    )   {   emit openTaskDetailsAsked       ( vm );   }
    void openRiskDetailsAsk        (   RiskViewModel       *vm    )   {   emit openRiskDetailsAsked       ( vm );   }

    void openContactEditAsk        (   ContactViewModel    *vm    )   {   emit openContactEditAsked    ( vm );      }
    void openQuestionEditAsk       (   QuestionViewModel   *vm    )   {   emit openQuestionEditAsked   ( vm );      }
    void openTaskEditAsk           (   TaskViewModel       *vm    )   {   emit openTaskEditAsked       ( vm );      }
    void openRiskEditAsk           (   RiskViewModel       *vm    )   {   emit openRiskEditAsked       ( vm );      }
};

#endif // VIEWMODELSBRIDGE_H
