#ifndef TASKLISTVIEWMODEL_H
#define TASKLISTVIEWMODEL_H

#include <QObject>
#include <QList>
#include <core/viewmodel.h>
#include <repositories/modelrepository.h>
#include <models/taskmodel.h>
#include <models/tasklistmodel.h>
#include <business/rules/taskrules.h>
#include <viewModels/taskviewmodel.h>

class TaskListViewModel : public core::ViewModel
{
    Q_OBJECT
    Q_PROPERTY(QList<QObject *> taskListViewModel READ taskListViewModel WRITE setTaskListViewModel NOTIFY taskListViewModelChanged)
    QList<QObject *> m_taskListViewModel;
    TaskListModel * m_Model;
public:
    explicit TaskListViewModel(QObject *parent = nullptr);
    explicit TaskListViewModel(TaskListModel *model, QObject *parent = nullptr);

    QList<QObject *> taskListViewModel() const
    {
        return m_taskListViewModel;
    }

    Q_INVOKABLE bool load   (long id=-1)    ;
    Q_INVOKABLE bool refresh()              ;
    Q_INVOKABLE bool store  ()              ;
    Q_INVOKABLE bool cancel ()              ;
signals:

        void taskListViewModelChanged(QList<QObject *> taskListViewModel);

public slots:
    void setTaskListViewModel(QList<QObject *> taskListViewModel)
    {
        if (m_taskListViewModel == taskListViewModel)
            return;

        m_taskListViewModel = taskListViewModel;
        emit taskListViewModelChanged(m_taskListViewModel);
    }

protected:
    bool populateViewModel();
    bool populateModel();
};

#endif // TASKLISTVIEWMODEL_H
