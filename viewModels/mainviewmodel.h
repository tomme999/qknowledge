#ifndef MAINVIEWMODEL_H
#define MAINVIEWMODEL_H

#include <QObject>
#include <core/viewmodel.h>
#include <viewModels/contactviewmodel.h>
#include <viewModels/riskviewmodel.h>
#include <viewModels/taskviewmodel.h>
#include <viewModels/questionviewmodel.h>

class MainViewModel : public core::ViewModel
{
    Q_OBJECT
protected:
    explicit MainViewModel(QObject *parent = nullptr);
    ~MainViewModel() {  if(m_mainViewModel==nullptr) delete m_mainViewModel ;}
    static MainViewModel *m_mainViewModel;

public:
    static MainViewModel * get() { if(m_mainViewModel==nullptr) { m_mainViewModel = new MainViewModel(); } return m_mainViewModel;}

signals:

    /* signal to create a new view */
    void openContactDetailsAsked        (   ContactViewModel    *vm     );
    void openQuestionDetailsAsked       (   QuestionViewModel   *vm     );
    void openTaskDetailsAsked           (   TaskViewModel       *vm     );
    void openRiskDetailsAsked           (   RiskViewModel       *vm     );

    void openContactEditAsked           (   ContactViewModel    *vm     );
    void openQuestionEditAsked          (   QuestionViewModel   *vm     );
    void openTaskEditAsked              (   TaskViewModel       *vm     );
    void openRiskEditAsked              (   RiskViewModel       *vm     );

public slots:

    /* slot to ask a new view */
    void openContactDetailsAsk     (   ContactViewModel    *vm    )   {   emit openContactDetailsAsked    ( vm );   }
    void openQuestionDetailsAsk    (   QuestionViewModel   *vm    )   {   emit openQuestionDetailsAsked   ( vm );   }
    void openTaskDetailsAsk        (   TaskViewModel       *vm    )   {   emit openTaskDetailsAsked       ( vm );   }
    void openRiskDetailsAsk        (   RiskViewModel       *vm    )   {   emit openRiskDetailsAsked       ( vm );   }

    void openContactEditAsk        (   ContactViewModel    *vm    )   {   emit openContactEditAsked    ( vm );      }
    void openQuestionEditAsk       (   QuestionViewModel   *vm    )   {   emit openQuestionEditAsked   ( vm );      }
    void openTaskEditAsk           (   TaskViewModel       *vm    )   {   emit openTaskEditAsked       ( vm );      }
    void openRiskEditAsk           (   RiskViewModel       *vm    )   {   emit openRiskEditAsked       ( vm );      }

public:
    // ViewModel interface
    bool load(long id);
    bool store();
    bool cancel();
    bool refresh();
};

#endif // MAINVIEWMODEL_H
