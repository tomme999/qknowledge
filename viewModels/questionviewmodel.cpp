#include "questionviewmodel.h"

QuestionViewModel::QuestionViewModel(QObject *parent) : core::ViewModel(parent)
{

}
QuestionViewModel::QuestionViewModel(QuestionModel *model, QObject *parent) :
    core::ViewModel (parent),
    m_Model( model )
{
    this->populateViewModel();
}

bool QuestionViewModel::load(long id)
{
    if(m_Model->load(id))
        return this->populateViewModel();
    return false;
}

bool QuestionViewModel::store()
{
//    if(this->populateModel())
//        return m_Model->store();
    return false;
}

bool QuestionViewModel::cancel()
{
    return true;
}

bool QuestionViewModel::refresh()
{
    return this->populateViewModel();
}

bool QuestionViewModel::populateViewModel()
{
    if(m_Model->question().isNull())
        return false;

    auto model = m_Model->question();

    //this->setId         ( model->id()           );
    this->setQuestion     ( model->question()     );
    this->setAnswer       ( model->answer()       );

    return true;
}

bool QuestionViewModel::populateModel()
{
//    // desynchronisation between model and viewmodel
//    if(m_Model->project()->id()!=this->id())
//        return false;

//    auto model = m_Model->project();

//    model->setName         ( this->name()          );
//    model->setDescription  ( this->description()   );

    return true;
}
