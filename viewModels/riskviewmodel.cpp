#include "riskviewmodel.h"

RiskViewModel::RiskViewModel(QObject *parent) : core::ViewModel(parent)
{

}

RiskViewModel::RiskViewModel(RiskModel *model, QObject *parent) :
    core::ViewModel (parent),
    m_Model( model )
{
    this->populateViewModel();
}


bool RiskViewModel::load(long id)
{
    if(m_Model->load(id))
        return this->populateViewModel();
    return false;
}
bool RiskViewModel::store()
{
//    if(this->populateModel())
//        return m_Model->store();
    return false;
}

bool RiskViewModel::cancel()
{
    return true;
}

bool RiskViewModel::refresh()
{
    return this->populateViewModel();
}

bool RiskViewModel::populateViewModel()
{
    if(m_Model->risk().isNull())
        return false;

    auto model = m_Model->risk();

    this->setStartTime      ( model->startDate()     );
    this->setName           ( model->name()          );
    this->setDescription    ( model->description()   );
    this->setIdContactOwner ( model->idContactOwner());
    this->setActionToLead   ( model->actionToLead()  );
    this->setIsClosed       ( model->isClosed()      );
    this->setEndDateWanted  ( model->endDateWanted() );
    this->setLevel          ( model->level()         );
    this->setProbability    ( model->probability()   );

    return true;
}

bool RiskViewModel::populateModel()
{
//    // desynchronisation between model and viewmodel
//    if(m_Model->project()->id()!=this->id())
//        return false;

//    auto model = m_Model->project();

//    model->setName         ( this->name()          );
//    model->setDescription  ( this->description()   );

    return true;
}
