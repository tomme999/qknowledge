#ifndef TASKVIEWMODEL_H
#define TASKVIEWMODEL_H

#include <QObject>
#include <QColor>
#include <core/viewmodel.h>
#include <repositories/modelrepository.h>
#include <models/taskmodel.h>
#include <business/rules/taskrules.h>
#include <repositories/constantrepository.h>
#include <business/ext/businessprojectrules.h>

class TaskViewModel : public core::ViewModel
{
    Q_OBJECT
    Q_PROPERTY(long         id              READ    id              WRITE setId             NOTIFY idChanged                )
    Q_PROPERTY(QString      name            READ    name            WRITE setName           NOTIFY nameChanged              )
    Q_PROPERTY(QString      description     READ    description     WRITE setDescription    NOTIFY descriptionChanged       )
    Q_PROPERTY(QDate        endDateWanted   READ    endDateWanted   WRITE setEndDateWanted  NOTIFY endDateWantedChanged     )
    Q_PROPERTY(long         idRoleOwner     READ    idRoleOwner     WRITE setIdRoleOwner    NOTIFY idRoleOwnerChanged       )
    Q_PROPERTY(long         idProject       READ    idProject       WRITE setIdProject      NOTIFY idProjectChanged         )

    long                    m_id            ;
    QString                 m_name          ;
    QString                 m_description   ;
    QDate                   m_endDateWanted ;
    long                    m_idRoleOwner   ;
    long                    m_idProject     ;

public:
    explicit TaskViewModel(QObject *parent = nullptr);
    explicit TaskViewModel(TaskModel* model, QObject *parent = nullptr);

    long    id            () const      {   return  m_id            ;       }
    QString name          () const      {   return  m_name          ;       }
    QString description   () const      {   return  m_description   ;       }
    QDate   endDateWanted () const      {   return  m_endDateWanted ;       }
    long    idRoleOwner   () const      {   return  m_idRoleOwner   ;       }
    long    idProject     () const      {   return  m_idProject     ;       }

    Q_INVOKABLE bool load   (long id=-1)    ;
    Q_INVOKABLE bool store  ()              ;
    Q_INVOKABLE bool cancel ()              ;
    Q_INVOKABLE bool refresh()              ;

signals:

    void    idChanged                   (    long        id                 );
    void    nameChanged                 (    QString     name               );
    void    descriptionChanged          (    QString     description        );
    void    endDateWantedChanged        (    QDate       endDateWanted      );
    void    idRoleOwnerChanged          (    long        idRoleOwner        );
    void    idProjectChanged            (    long        idProject          );

public slots:

    void    setId                       (    long        id                 )    _VSETTER__( TaskRules,             id                 )
    void    setName                     (    QString     name               )    _VSETTER__( TaskRules,             name               )
    void    setDescription              (    QString     description        )    _VSETTER__( TaskRules,             description        )
    void    setEndDateWanted            (    QDate       endDateWanted      )    _VSETTER__( TaskRules,             endDateWanted      )
    void    setIdRoleOwner              (    long        idRoleOwner        )    _VSETTER__( TaskRules,             idRoleOwner        )
    void    setIdProject                (    long        idProject          )    _VSETTER__( BusinessProjectRules,  idProject          )

protected:
    bool populateViewModel();
    bool populateModel();

private:
    TaskModel    *m_Model;
};

#endif // TASKVIEWMODEL_H
