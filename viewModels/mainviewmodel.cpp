#include "mainviewmodel.h"
#include <repositories/viewmodelrepository.h>

MainViewModel *MainViewModel::m_mainViewModel = nullptr;

MainViewModel::MainViewModel(QObject *parent) : core::ViewModel(parent)
{
    connect(ViewModelRepository::instance().bridge(), &ViewModelsBridge::openRiskEditAsked, this,   &MainViewModel::openRiskEditAsk );
    connect(ViewModelRepository::instance().bridge(), &ViewModelsBridge::openQuestionEditAsked, this,   &MainViewModel::openQuestionEditAsk );
}

bool MainViewModel::load(long id)
{
    Q_UNUSED(id)

    return false;
}

bool MainViewModel::store()
{
    return false;
}

bool MainViewModel::cancel()
{
    return false;
}

bool MainViewModel::refresh()
{
    return false;
}
