#ifndef PROJECTLISTVIEWMODEL_H
#define PROJECTLISTVIEWMODEL_H

#include <QObject>
#include <QList>
#include <core/viewmodel.h>
#include <repositories/modelrepository.h>
#include <models/projectmodel.h>
#include <models/projectlistmodel.h>
#include <business/rules/projectrules.h>
#include <viewModels/projectviewmodel.h>

class ProjectListViewModel : public core::ViewModel
{
    Q_OBJECT
    Q_PROPERTY(QList<QObject *> projectListViewModel READ projectListViewModel WRITE setProjectListViewModel NOTIFY projectListViewModelChanged)
    QList<QObject *> m_projectListViewModel;
    ProjectListModel * m_Model;

public:
    explicit ProjectListViewModel(QObject *parent = nullptr);

    QList<QObject *> projectListViewModel() const
    {
        return m_projectListViewModel;
    }

    Q_INVOKABLE bool load   (long id=-1)    ;
    Q_INVOKABLE bool store  ()              ;
    Q_INVOKABLE bool cancel ()              ;
    Q_INVOKABLE bool refresh()              ;

signals:

    void projectListViewModelChanged(QList<QObject *> projectListViewModel);

public slots:
    void setProjectListViewModel(QList<QObject *> projectListViewModel)
    {
        if (m_projectListViewModel == projectListViewModel)
            return;

        m_projectListViewModel = projectListViewModel;
        emit projectListViewModelChanged(m_projectListViewModel);
    }

protected:
    bool populateViewModel();
    bool populateModel();
};

#endif // PROJECTLISTVIEWMODEL_H
