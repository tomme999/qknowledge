#include "projectdetailviewmodel.h"

ProjectDetailViewModel::ProjectDetailViewModel(QObject *parent)
    : ViewModel(parent)
    , m_Model(ModelRepository::instance().get<ProjectDetailsModel>(this))
{
    this->setRiskListViewModel(new RiskListViewModel(this->m_Model->riskListModel(), this ));
    this->setTaskListViewModel(new TaskListViewModel(this->m_Model->taskListModel(), this ));
    this->setQuestionListViewModel(new QuestionListViewModel(this->m_Model->questionListModel(), this ));
    this->setProjectViewModel(new ProjectViewModel(this->m_Model->projectModel(), this ));
}

bool ProjectDetailViewModel::load(long id)
{
    qDebug() << "ProjectDetailViewModel::load" << id;
    if(id!=-1 && m_Model->load(id))
        return this->populateViewModel();
    if( m_Model->load())
        return this->populateViewModel();
    return false;
}

bool ProjectDetailViewModel::store()
{
    qFatal("methode store() in ProjectDetailViewModel not implemented");
}

bool ProjectDetailViewModel::cancel()
{
    qFatal("methode cancel() in ProjectDetailViewModel not implemented");
}

bool ProjectDetailViewModel::refresh()
{
    return this->populateViewModel();
}

bool ProjectDetailViewModel::populateViewModel()
{

    //this->setRiskListViewModel(new RiskListViewModel(this->m_Model->RiskListModel(), this ));
    //this->setTaskListViewModel(new TaskListViewModel(this->m_Model->TaskListModel(), this ));
    //this->setQuestionListViewModel(new QuestionListViewModel(this->m_Model->QuestionListModel(), this ));
    m_projectViewModel->refresh();
    m_riskListViewModel->refresh();
    m_taskListViewModel->refresh();
    m_questionListViewModel->refresh();

    emit projectViewModelChanged        (   m_projectViewModel      );
    emit riskListViewModelChanged       (   m_riskListViewModel     );
    emit taskListViewModelChanged       (   m_taskListViewModel     );
    emit questionListViewModelChanged   (   m_questionListViewModel );

    return true;
}

bool ProjectDetailViewModel::populateModel()
{
    qFatal("methode populateModel() in ProjectDetailViewModel not implemented");
}
