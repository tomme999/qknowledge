#ifndef RISKLISTVIEWMODEL_H
#define RISKLISTVIEWMODEL_H

#include <QObject>
#include <QList>
#include <core/viewmodel.h>
#include <repositories/modelrepository.h>
#include <models/riskmodel.h>
#include <models/risklistmodel.h>
#include <business/rules/riskrules.h>
#include <viewModels/riskviewmodel.h>
#include <viewModels/mainviewmodel.h>

class RiskListViewModel : public core::ViewModel
{
    Q_OBJECT
    Q_PROPERTY(QList<QObject *> riskListViewModel READ riskListViewModel WRITE setRiskListViewModel NOTIFY riskListViewModelChanged)
    QList<QObject *> m_riskListViewModel;
    RiskListModel * m_Model;
public:
    explicit RiskListViewModel(QObject *parent = nullptr);
    explicit RiskListViewModel(RiskListModel *model, QObject *parent = nullptr);

    QList<QObject *> riskListViewModel() const
    {
        return m_riskListViewModel;
    }

    Q_INVOKABLE bool load   (long id=-1)    ;
    Q_INVOKABLE bool refresh()              ;
    Q_INVOKABLE bool store  ()              ;
    Q_INVOKABLE bool cancel ()              ;

    Q_INVOKABLE bool edit();

signals:

    void riskListViewModelChanged(QList<QObject *> riskListViewModel);

public slots:
    void setRiskListViewModel(QList<QObject *> riskListViewModel)
    {
        if (m_riskListViewModel == riskListViewModel)
            return;

        m_riskListViewModel = riskListViewModel;
        emit riskListViewModelChanged(m_riskListViewModel);
    }

protected:
    bool populateViewModel();
    bool populateModel();
};


#endif // RISKLISTVIEWMODEL_H
