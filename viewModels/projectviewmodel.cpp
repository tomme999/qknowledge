#include "projectviewmodel.h"

ProjectViewModel::ProjectViewModel(QObject *parent) :
    core::ViewModel (parent),
    m_Model( ModelRepository::instance().get<ProjectModel>(this) )
{

}

ProjectViewModel::ProjectViewModel(ProjectModel *model, QObject *parent) :
    core::ViewModel (parent),
    m_Model( model )
{
    this->populateViewModel();
}

bool ProjectViewModel::load(long id)
{
    if(m_Model->load(id))
        return this->populateViewModel();
    return false;
}

bool ProjectViewModel::store()
{
    if(this->populateModel())
        return m_Model->store();
    return false;
}

bool ProjectViewModel::cancel()
{
    return this->populateViewModel();
}

bool ProjectViewModel::refresh()
{
    return populateViewModel();
}

bool ProjectViewModel::populateViewModel()
{
    if(m_Model->project().isNull())
        return false;

    auto model = m_Model->project();

    this->setId         ( model->id()           );
    this->setName       ( model->name()         );
    this->setDescription( model->description()  );

    return true;
}

bool ProjectViewModel::populateModel()
{
    // desynchronisation between model and viewmodel
    if(m_Model->project()->id()!=this->id())
        return false;

    auto model = m_Model->project();

    model->setName         ( this->name()          );
    model->setDescription  ( this->description()   );

    return true;
}
