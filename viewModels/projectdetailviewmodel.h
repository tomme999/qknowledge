#ifndef PROJECTDETAILVIEWMODEL_H
#define PROJECTDETAILVIEWMODEL_H

#include <QObject>
#include <QList>
#include <core/viewmodel.h>
#include <repositories/modelrepository.h>
#include <models/projectdetailsmodel.h>
#include <models/projectlistmodel.h>
#include <models/risklistmodel.h>
#include <models/tasklistmodel.h>
#include <viewModels/projectlistviewmodel.h>
#include <viewModels/risklistviewmodel.h>
#include <viewModels/questionlistviewmodel.h>
#include <viewModels/tasklistviewmodel.h>


class ProjectDetailViewModel : public core::ViewModel
{
    Q_OBJECT
    Q_PROPERTY(ProjectViewModel      * projectViewModel       READ projectViewModel       WRITE setProjectViewModel       NOTIFY projectViewModelChanged)
    Q_PROPERTY(RiskListViewModel     * riskListViewModel      READ riskListViewModel      WRITE setRiskListViewModel      NOTIFY riskListViewModelChanged)
    Q_PROPERTY(TaskListViewModel     * taskListViewModel      READ taskListViewModel      WRITE setTaskListViewModel      NOTIFY taskListViewModelChanged)
    Q_PROPERTY(QuestionListViewModel * questionListViewModel  READ questionListViewModel  WRITE setQuestionListViewModel  NOTIFY questionListViewModelChanged)

    ProjectDetailsModel * m_Model;

    ProjectViewModel        * m_projectViewModel;
    RiskListViewModel       * m_riskListViewModel;
    TaskListViewModel       * m_taskListViewModel;
    QuestionListViewModel   * m_questionListViewModel;

    Q_INVOKABLE bool load   (long id=-1)    ;
    Q_INVOKABLE bool store  ()              ;
    Q_INVOKABLE bool cancel ()              ;
    Q_INVOKABLE bool refresh()              ;

public:
    explicit ProjectDetailViewModel(QObject *parent = nullptr);

    ProjectViewModel * projectViewModel() const         {   return m_projectViewModel;              }
    RiskListViewModel * riskListViewModel() const       {   return m_riskListViewModel;             }
    TaskListViewModel * taskListViewModel() const       {   return m_taskListViewModel;             }
    QuestionListViewModel * questionListViewModel() const   {   return m_questionListViewModel;     }

signals:

    void projectViewModelChanged       ( ProjectViewModel     * projectViewModel      );
    void riskListViewModelChanged      ( RiskListViewModel    * riskListViewModel     );
    void taskListViewModelChanged      ( TaskListViewModel    * taskListViewModel     );
    void questionListViewModelChanged  ( QuestionListViewModel    * questionListViewModel );

protected slots:

    void setProjectViewModel(ProjectViewModel *projectViewModel)
    {
        if (m_projectViewModel == projectViewModel) return;

        m_projectViewModel = projectViewModel;
        emit projectViewModelChanged(m_projectViewModel);
    }

    void setRiskListViewModel(RiskListViewModel *riskListViewModel)
    {
        if (m_riskListViewModel == riskListViewModel) return;

        m_riskListViewModel = riskListViewModel;
        emit riskListViewModelChanged(m_riskListViewModel);
    }
    void setTaskListViewModel(TaskListViewModel *taskListViewModel)
    {
        if (m_taskListViewModel == taskListViewModel)   return;

        m_taskListViewModel = taskListViewModel;
        emit taskListViewModelChanged(m_taskListViewModel);
    }
    void setQuestionListViewModel(QuestionListViewModel *questionListViewModel)
    {
        if (m_questionListViewModel == questionListViewModel)   return;

        m_questionListViewModel = questionListViewModel;
        emit questionListViewModelChanged(m_questionListViewModel);
    }

protected:
    bool populateViewModel();
    bool populateModel();
};

#endif // PROJECTDETAILVIEWMODEL_H
