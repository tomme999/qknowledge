#ifndef CONTACTVIEWMODEL_H
#define CONTACTVIEWMODEL_H

#include <QObject>
#include <core/viewmodel.h>
#include <repositories/modelrepository.h>
#include <models/contactmodel.h>
#include <business/rules/contactrules.h>
#include <repositories/constantrepository.h>

class ContactViewModel : public core::ViewModel
{
    Q_OBJECT
    Q_PROPERTY(long     id              READ id             WRITE setId             NOTIFY idChanged)
    Q_PROPERTY(QString  name            READ name           WRITE setName           NOTIFY nameChanged)
    Q_PROPERTY(QString  firstName       READ firstName      WRITE setFirstName      NOTIFY firstNameChanged)
    Q_PROPERTY(QString  phoneNumber     READ phoneNumber    WRITE setPhoneNumber    NOTIFY phoneNumberChanged)
    Q_PROPERTY(QString  email           READ email          WRITE setEmail          NOTIFY emailChanged)
    Q_PROPERTY(QColor   backgroundColor READ backgroundColor CONSTANT)
    Q_PROPERTY(QString  initials        READ initials                               NOTIFY nameChanged)

    long    m_id;
    QString m_name;
    QString m_firstName;
    QString m_phoneNumber;
    QString m_email;
    QColor  m_backgroundColor;

public:

    explicit ContactViewModel(QObject *parent = nullptr);
    explicit ContactViewModel(ContactModel *model, QObject *parent = nullptr);

    long    id()            const   {    return m_id;           }
    QString name()          const   {    return m_name;         }
    QString firstName()     const   {    return m_firstName;    }
    QString phoneNumber()   const   {    return m_phoneNumber;  }
    QString email()         const   {    return m_email;        }
    QColor  backgroundColor() const {   return ConstantRepository::instance().getColorFromId(m_id); }
    QString initials()      const   {   return ContactRules::constactInitials(m_firstName, m_name); }

    Q_INVOKABLE bool load   (long id=-1)    ;
    Q_INVOKABLE bool store  ()              ;
    Q_INVOKABLE bool cancel ()              ;
    Q_INVOKABLE bool refresh()              ;


signals:
    void idChanged                  (   long id                 );
    void nameChanged                (   QString name            );
    void firstNameChanged           (   QString firstName       );
    void phoneNumberChanged         (   QString phoneNumber     );
    void emailChanged               (   QString email           );

public slots:
    void setId                      (   long    id              )    _VSETTER__(    ContactRules,   id              )
    void setName                    (   QString name            )    _VSETTER__(    ContactRules,   name            )
    void setFirstName               (   QString firstName       )    _VSETTER__(    ContactRules,   firstName       )
    void setPhoneNumber             (   QString phoneNumber     )    _VSETTER__(    ContactRules,   phoneNumber     )
    void setEmail                   (   QString email           )    _VSETTER__(    ContactRules,   email           )

protected:
    bool populateViewModel();
    bool populateModel();

private:
    ContactModel    *m_Model;
};

#endif // CONTACTVIEWMODEL_H
