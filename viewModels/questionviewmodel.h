#ifndef QUESTIONVIEWMODEL_H
#define QUESTIONVIEWMODEL_H

#include <QDate>
#include <QObject>
#include <core/viewmodel.h>
#include <repositories/modelrepository.h>
#include <models/riskmodel.h>
#include <business/rules/riskrules.h>
#include <business/ext/businessprojectrules.h>

class QuestionViewModel : public core::ViewModel
{
    Q_OBJECT

    Q_PROPERTY(QString  question        READ question       WRITE setQuestion       NOTIFY questionChanged)     // Intitule de la question
    Q_PROPERTY(QString  answer          READ answer         WRITE setAnswer         NOTIFY answerChanged)       // Reponse de la question
    Q_PROPERTY(QDate    answerDate      READ answerDate     WRITE setAnswerDate     NOTIFY answerDateChanged)   // Date de la reponse
    Q_PROPERTY(QDate    wanted          READ wanted         WRITE setWanted         NOTIFY wantedChanged)       // Date souhaite pour la reponse
    Q_PROPERTY(long     idProject       READ idProject      WRITE setIdProject      NOTIFY idProjectChanged) // Id du projet

    QString m_question;
    QString m_answer;
    QDate   m_answerDate;
    QDate   m_wanted;
    long    m_idProject;

public:
    explicit QuestionViewModel(QObject *parent = nullptr);
    explicit QuestionViewModel(QuestionModel* model, QObject *parent = nullptr);

    QString question()      const   {   return m_question;      }
    QString answer()        const   {   return m_answer;        }
    QDate   answerDate()    const   {   return m_answerDate;    }
    QDate   wanted()        const   {   return m_wanted;        }
    long    idProject()     const   {   return m_idProject;     }

    Q_INVOKABLE bool load   (long id=-1)    ;
    Q_INVOKABLE bool store  ()              ;
    Q_INVOKABLE bool cancel ()              ;
    Q_INVOKABLE bool refresh()              ;

signals:

    void questionChanged            (   QString question        );
    void answerChanged              (   QString answer          );
    void answerDateChanged          (   QDate answerDate        );
    void wantedChanged              (   QDate wanted            );
    void idProjectChanged           (   long idProject          );

public slots:

    void setQuestion                (   QString question        )    _VSETTER__(   QuestionRules,           question        )
    void setAnswer                  (   QString answer          )    _VSETTER__(   QuestionRules,           answer          )
    void setAnswerDate              (   QDate answerDate        )    _VSETTER__(   QuestionRules,           answerDate      )
    void setWanted                  (   QDate wanted            )    _VSETTER__(   QuestionRules,           wanted          )
    void setIdProject               (   long idProject          )    _VSETTER__(   BusinessProjectRules,    idProject       )

protected:
    bool populateViewModel();
    bool populateModel();

private:
    QuestionModel    *m_Model;
};

#endif // QUESTIONVIEWMODEL_H
