#ifndef PROJECTVIEWMODEL_H
#define PROJECTVIEWMODEL_H

#include <QObject>
#include <QColor>
#include <core/viewmodel.h>
#include <repositories/modelrepository.h>
#include <models/projectmodel.h>
#include <business/rules/projectrules.h>
#include <repositories/constantrepository.h>

class ProjectViewModel : public core::ViewModel
{
    Q_OBJECT
    Q_PROPERTY(long     id              READ id             WRITE setId             NOTIFY idChanged)
    Q_PROPERTY(QString  name            READ name           WRITE setName           NOTIFY nameChanged)
    Q_PROPERTY(QString  description     READ description    WRITE setDescription    NOTIFY descriptionChanged)
    Q_PROPERTY(QString  initials        READ initials                               NOTIFY nameChanged)
    Q_PROPERTY(QColor   backgroundColor READ backgroundColor CONSTANT)

    long    m_id;
    QString m_name;
    QString m_description;
    QColor  m_backgroundColor;

public:
    explicit ProjectViewModel(QObject *parent = nullptr);
    explicit ProjectViewModel(ProjectModel* model, QObject *parent = nullptr);

    long    id()            const   {   return m_id;           }
    QString name()          const   {   return m_name;         }
    QString description()   const   {   return m_description;  }
    QColor  backgroundColor() const {   return ConstantRepository::instance().getColorFromId(m_id); }
    QString initials()      const   {   return ProjectRules::projectInitials(m_name); }

    Q_INVOKABLE bool load   (long id=-1)    ;
    Q_INVOKABLE bool store  ()              ;
    Q_INVOKABLE bool cancel ()              ;
    Q_INVOKABLE bool refresh()              ;

signals:

    void idChanged                  (   long id                 );
    void nameChanged                (   QString name            );
    void descriptionChanged         (   QString description     );

public slots:

    void setId                      (   long    id              )    _VSETTER__(    ProjectRules,   id              )
    void setName                    (   QString name            )    _VSETTER__(    ProjectRules,   name            )
    void setDescription             (   QString description     )    _VSETTER__(    ProjectRules,   description     )

protected:
    bool populateViewModel();
    bool populateModel();

private:
    ProjectModel    *m_Model;
};

#endif // PROJECTVIEWMODEL_H
