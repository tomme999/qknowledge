#include "tasklistviewmodel.h"

TaskListViewModel::TaskListViewModel(QObject *parent) : ViewModel(parent),
    m_Model(ModelRepository::instance().get<TaskListModel>(this))
{

}

TaskListViewModel::TaskListViewModel(TaskListModel *model, QObject *parent)
    : ViewModel(parent),
    m_Model(model)
{

}
bool TaskListViewModel::load(long id)
{
    if(id!=-1 && m_Model->load(id))
        return this->populateViewModel();
    if( m_Model->load())
        return this->populateViewModel();
    return false;
}

bool TaskListViewModel::refresh()
{
    return this->populateViewModel();
}


bool TaskListViewModel::store()
{
    qFatal("methode store() in QuestionListViewModel not implemented");
}

bool TaskListViewModel::cancel()
{
    qFatal("methode cancel() in QuestionListViewModel not implemented");
}

bool TaskListViewModel::populateViewModel()
{
    m_taskListViewModel.clear();
    foreach(TaskService::ShrPtr u, this->m_Model->tasksList())
    {
        //auto a = new ProjectViewModel(u.data(),this);
        TaskModel * taskModel = new TaskModel(u, this);
        TaskViewModel * taskViewModel = new TaskViewModel(taskModel, this);

        m_taskListViewModel <<  taskViewModel;
    }
    emit taskListViewModelChanged( m_taskListViewModel );

    return true;
}

bool TaskListViewModel::populateModel()
{
    qFatal("methode populateModel() in QuestionListViewModel not implemented");
}
