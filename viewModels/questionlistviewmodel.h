#ifndef QUESTIONLISTVIEWMODEL_H
#define QUESTIONLISTVIEWMODEL_H

#include <QObject>
#include <QList>
#include <core/viewmodel.h>
#include <repositories/modelrepository.h>
#include <models/questionmodel.h>
#include <models/questionlistmodel.h>
#include <business/rules/questionrules.h>
#include <viewModels/questionviewmodel.h>

class QuestionListViewModel : public core::ViewModel
{
    Q_OBJECT
    Q_PROPERTY(QList<QObject *> questionListViewModel READ questionListViewModel WRITE setQuestionListViewModel NOTIFY questionListViewModelChanged)
    QList<QObject *> m_questionListViewModel;
    QuestionListModel * m_Model;
public:
    explicit QuestionListViewModel(QObject *parent = nullptr);
    explicit QuestionListViewModel(QuestionListModel *model,QObject *parent = nullptr);

    QList<QObject *> questionListViewModel() const
    {
        return m_questionListViewModel;
    }

    Q_INVOKABLE bool load   (long id=-1)    ;
    Q_INVOKABLE bool refresh()              ;
    Q_INVOKABLE bool store  ()              ;
    Q_INVOKABLE bool cancel ()              ;
    Q_INVOKABLE bool edit()                 ;

signals:

    void questionListViewModelChanged(QList<QObject *> questionListViewModel);

public slots:
    void setQuestionListViewModel(QList<QObject *> questionListViewModel)
    {
        if (m_questionListViewModel == questionListViewModel)
            return;

        m_questionListViewModel = questionListViewModel;
        emit questionListViewModelChanged(m_questionListViewModel);
    }

protected:
    bool populateViewModel();
    bool populateModel();
};

#endif // QUESTIONLISTVIEWMODEL_H
