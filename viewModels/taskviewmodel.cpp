#include "taskviewmodel.h"

TaskViewModel::TaskViewModel(QObject *parent) : ViewModel(parent),
    m_Model( ModelRepository::instance().get<TaskModel>(this) )
{

}

TaskViewModel::TaskViewModel(TaskModel *model, QObject *parent) :
    core::ViewModel (parent),
    m_Model( model )
{
    this->populateViewModel();
}


bool TaskViewModel::load(long id)
{
    if(m_Model->load(id))
        return this->populateViewModel();
    return false;
}

bool TaskViewModel::store()
{
    if(this->populateModel())
        return m_Model->store();
    return false;
}

bool TaskViewModel::cancel()
{
    return true;
}

bool TaskViewModel::refresh()
{
    return this->populateViewModel();
}

bool TaskViewModel::populateViewModel()
{
    if(m_Model->task().isNull())
        return false;

    auto model = m_Model->task();

    this->setId             ( model->id()           );
    this->setName           ( model->name()         );
    this->setDescription    ( model->description()  );
    this->setEndDateWanted  ( model->endDateWanted());
    this->setIdRoleOwner    ( model->idRoleOwner()  );
    this->setIdProject      ( model->idProject()    );

    return true;
}

bool TaskViewModel::populateModel()
{
    // desynchronisation between model and viewmodel
    if(m_Model->task()->id()!=this->id())
        return false;

    auto model = m_Model->task();

    model->setId             ( this->id()           );
    model->setName           ( this->name()         );
    model->setDescription    ( this->description()  );
    model->setEndDateWanted  ( this->endDateWanted());
    model->setIdRoleOwner    ( this->idRoleOwner()  );
    model->setIdProject      ( this->idProject()    );

    return true;
}
