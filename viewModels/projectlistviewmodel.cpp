#include "projectlistviewmodel.h"

ProjectListViewModel::ProjectListViewModel(QObject *parent)
    : ViewModel(parent),
      m_Model(ModelRepository::instance().get<ProjectListModel>(this))
{

}

bool ProjectListViewModel::load(long id)
{
    if(id!=-1 && m_Model->load(id))
        return this->populateViewModel();
    if( m_Model->load())
        return this->populateViewModel();
    return false;
}

bool ProjectListViewModel::store()
{
    qFatal("methode store() in ProjectListViewModel not implemented");
}

bool ProjectListViewModel::cancel()
{
    qFatal("methode cancel() in ProjectListViewModel not implemented");
}

bool ProjectListViewModel::refresh()
{
    return this->populateViewModel();
}

bool ProjectListViewModel::populateViewModel()
{
    m_projectListViewModel.clear();
    foreach(ProjectService::ShrPtr u, this->m_Model->projectsList())
    {
        //auto a = new ProjectViewModel(u.data(),this);
        ProjectModel * projectModel = new ProjectModel(u, this);
        ProjectViewModel * projectViewModel = new ProjectViewModel(projectModel, this);

        m_projectListViewModel <<  projectViewModel;
    }
    emit projectListViewModelChanged( m_projectListViewModel );

    return true;
}

bool ProjectListViewModel::populateModel()
{
    qFatal("methode populateModel() in ProjectListViewModel not implemented");
}
