#ifndef CONTACTLISTVIEWMODEL_H
#define CONTACTLISTVIEWMODEL_H

#include <QObject>
#include <QList>
#include <core/viewmodel.h>
#include <models/contactlistmodel.h>
#include <models/contactmodel.h>
#include <viewModels/contactviewmodel.h>

class ContactListViewModel : public core::ViewModel
{
    Q_OBJECT
    Q_PROPERTY(QList<QObject *> contactListViewModel READ contactListViewModel WRITE setContactListViewModel NOTIFY contactListViewModelChanged)

    ContactListModel * m_Model;
    QList<QObject *> m_contactListViewModel;

public:
    explicit ContactListViewModel(QObject *parent = nullptr);

    QList<QObject *> contactListViewModel() const
    {
        return m_contactListViewModel;
    }

    Q_INVOKABLE bool load   (long id=-1)    ;
    Q_INVOKABLE bool store  ()              ;
    Q_INVOKABLE bool cancel ()              ;
    Q_INVOKABLE bool refresh()              ;
signals:

    void contactListViewModelChanged(QList<QObject *> contactListViewModel);

public slots:
    void setContactListViewModel(QList<QObject *> contactListViewModel)
    {
        if (m_contactListViewModel == contactListViewModel)
            return;

        m_contactListViewModel = contactListViewModel;
        emit contactListViewModelChanged(m_contactListViewModel);
    }
protected:
    bool populateViewModel();
    bool populateModel();

    // ViewModel interface

};

#endif // CONTACTLISTVIEWMODEL_H
