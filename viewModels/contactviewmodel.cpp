#include "contactviewmodel.h"

ContactViewModel::ContactViewModel(QObject *parent) :
    core::ViewModel (parent),
    m_Model( ModelRepository::instance().get<ContactModel>(this) )
{

}

ContactViewModel::ContactViewModel(ContactModel *model, QObject *parent) :
    core::ViewModel (parent),
    m_Model( model )
{
    this->populateViewModel();
}

bool ContactViewModel::load(long id)
{
    if(m_Model->load(id))
        return this->populateViewModel();
    return false;
}

bool ContactViewModel::store()
{
    if(this->populateModel())
        return m_Model->store();
    return false;
}

bool ContactViewModel::cancel()
{
    return true;
}

bool ContactViewModel::refresh()
{
    return this->populateViewModel();
}

bool ContactViewModel::populateViewModel()
{
    if(m_Model->contact().isNull())
        return false;

    auto model = m_Model->contact();

    this->setId         ( model->id()           );
    this->setName       ( model->name()         );
    this->setFirstName  ( model->firstName()    );
    this->setPhoneNumber( model->phoneNumber()  );
    this->setEmail      ( model->email()        );

    return true;
}

bool ContactViewModel::populateModel()
{
    // desynchronisation between model and viewmodel
    if(m_Model->contact()->id()!=this->id())
        return false;

    auto model = m_Model->contact();

    model->setName         ( this->name()          );
    model->setFirstName    ( this->firstName()     );
    model->setPhoneNumber  ( this->phoneNumber()   );
    model->setEmail        ( this->email()         );

    return true;
}
