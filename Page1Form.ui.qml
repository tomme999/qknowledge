import QtQuick 2.9
import QtQuick.Controls 2.2
//import QtQuick.Controls.Styles.Desktop 1.0
import "views"
import "qmlControls"
import core.backend.validator 1.0

Page {
    width: 600
    height: 400

    title: qsTr("Page 1")

    Label {
        text: qsTr("You are on Page 1.")
        anchors.verticalCenterOffset: -53
        anchors.horizontalCenterOffset: -200
        anchors.centerIn: parent
    }

    Button {
        id: button
        x: 27
        y: 23
        text: qsTr("Button")
    }

    DelayButton {
        id: delayButton
        x: 43
        y: 83
        text: qsTr("Delay Button")
    }

    Slider {
        id: slider
        x: 27
        y: 171
        value: 0.5
    }

    SwitchDelegate {
        id: switchDelegate
        x: 19
        y: 256
        text: qsTr("Switch Delegate")
    }

    Switch {
        id: element
        x: 27
        y: 213
        text: qsTr("Switch")
    }

    QuestionViewForm {
        id: questionViewForm
        x: 194
        y: 46
        width: 266
        height: 58

        TextField {
            id: textField
            x: 155
            y: 50
            text: qsTr("Text Field")
        }
    }
    LabelText {
        id: labelText
        x: 296
        y: 221
        width: 150
        height: 77
        text: "22/03/2019"
        inputMask: qsTr("99/99/9999")
        validator: DateValidator {
        }
    }
    LabelText {
        id: labelTextDateTime
        x: 267
        y: 291
        width: 150
        height: 77
        text: "22/03/2019"
        inputMask: qsTr("99/99/9999 99:99")
        validator: DateTimeValidator {
        }
    }
}
